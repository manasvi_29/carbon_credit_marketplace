const {ethers} = require('ethers')

 const CarbonCreditTokenABI = require('../abis/CarbonCreditTokenABI.json')
 const GreenNFTABI = require('../abis/GreenNFTABI.json')
 const GreenNFTDATAABI = require('../abis/GreenNFTDATAABI.json')
 const GreenNftFactoryAbi = require('../abis/GreenNftFactoryAbi')
 const GreenNftMarketplaceAbi = require('../abis/GreenNftMarketplaceAbi.json')

 const carbonCreditTokenaddress = "0x940E540Cb6b952e51b87C05655302B241e126b3C"
 const greenNFTDataaddress = "0xd06550B132359c63eAD2eEE922372D74a1C23bE7"
 const greenNFTMarketplaceaddress = "0xEe226b201D3F846617a5CE3B28be2B1696B9078F"
 const greenNFTFactoryaddress = "0xEe9E85057d463930c4dF379Beb6Cb3cd757ED5ac"

const polygonTestnetUrl = "https://polygon-mumbai.g.alchemy.com/v2/OA4nH2rIMd4NFZYWsUdiCTrLH8WQWk6L";
 const provider = new ethers.providers.JsonRpcProvider(polygonTestnetUrl);


// const carbonCreditTokenContract = new ethers.Contract(carbonCreditTokenaddress, CarbonCreditTokenABI, provider);
// const greenNFTDataContract = new ethers.Contract(carbonCreditTokenaddress, CarbonCreditTokenABI, provider);
// const greenNFTMarketplaceaddress = new ethers.Contract(carbonCreditTokenaddress, CarbonCreditTokenABI, provider);
// const greenNFTFactoryaddress = new ethers.Contract(carbonCreditTokenaddress, CarbonCreditTokenABI, provider);

module.exports = {CarbonCreditTokenABI,GreenNFTABI,GreenNFTDATAABI,GreenNftFactoryAbi,GreenNftMarketplaceAbi,carbonCreditTokenaddress,greenNFTDataaddress,greenNFTMarketplaceaddress,greenNFTFactoryaddress,provider}