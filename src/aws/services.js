const aws = require("aws-sdk");
const awsConfig = require("../configs/serverConfigs.json").AWS_CONFIG;
const randomString = require("randomstring");


aws.config.update({
  accessKeyId: awsConfig.access_key,
  secretAccessKey: awsConfig.secret_key,
  region: awsConfig.region,
});

const sendOTP = function (number, otp) {
  return new Promise((resolve, reject) =>{
    const sns = new aws.SNS();
  const params = {
    Message: `Your OTP for account verification in carbon credit marketplace is ${otp}`,
    PhoneNumber: `+91${number}`,
  };
  sns.publish(params, (err, data) => {
    if (data) {
      resolve(data);
    } else {
      reject(err);
    }
  });
  })
  
};


const forgotPasswordOTP = function(data, otp){
  return new Promise((resolve, reject) =>{
    const sns = new aws.SNS();
    const params = {
    Message: 'Hello' + " "+
    data.name
    + '\n\nYou are receiving this message from carbon credit marketplace because you requested to reset your password.\n\n'
    +'Here is your otp to reset your password - '+
    otp+
    '\n\n If you did not request a password reset, please ignore this message.'+
    '\n\n Thankyou',
    PhoneNumber: `+91${data.mobile_number}`,
  };
  sns.publish(params, (err, data) => {
    if (data) {
      resolve(data)
    } else {
      reject(err);
    }
  });
  })
    
}

const resetPasswordMsg = function(data){
  return new Promise((resolve, reject) =>{
    const sns = new aws.SNS();
    const params = {
    Message: 'Hello,\n\n' +
    'This is a confirmation that the password for your account on carbon credit marketplace ' + data.email_id + ' has been just changed.\n',
    PhoneNumber: `+91${data.mobile_number}`,
  };
  sns.publish(params, (err, data) => {
    if (data) {
      resolve(data)
    } else {
        reject(err)
    }
  });
  })
  
}

const sendCredentials = function(data){
  return new Promise((resolve, reject) => {
    const sns = new aws.SNS();
    const params = {
      Message: 'Hello,\n\n' +
        'Welcome to the carbon credit marketplace.\n' +
        'Below is your Username and Password.\n\n'+
        `Username: ${data.email_id} or ${data.name}\n` + 
        `Password: ${data.password}`,
      PhoneNumber: `+91${data.mobile_number}`,
    };
    sns.publish(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}




module.exports = { sendOTP,forgotPasswordOTP,resetPasswordMsg,sendCredentials };
