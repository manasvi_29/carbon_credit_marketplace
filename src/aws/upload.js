const azureConfig = require("../configs/serverConfigs.json").AZURE_CONFIG;
const { BlobServiceClient } = require("@azure/storage-blob");

const blobServiceClient = BlobServiceClient.fromConnectionString(
  azureConfig.connect_string
);

// Upload the blob data to the block blob
async function uploadFiles(file) {
  const blobName =
    file.originalname.replace(/[^a-zA-Z0-9]/g, "") + new Date().getTime();
  const containerClient = blobServiceClient.getContainerClient(
    azureConfig.container_name
  );

  const blobURL = `https://${azureConfig.account_name}.blob.core.windows.net/${azureConfig.container_name}/${blobName}`;
  // console.log(blobSAS);

  const blockBlobClient = containerClient.getBlockBlobClient(blobName);
    let type = file.mimetype
  // blockBlobClient.setHTTPHeaders({'content-type':type});
  const uploadBlobResponse = await blockBlobClient.upload(
    file.buffer,
    file.buffer.length
  );
  return { uploadBlobResponse, blobURL };
}


module.exports = { uploadFiles };
