const soilDetailModel = require("../models/soilDetailModel");


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns Data and state of the api
 * @function From mongo obtain the data with UUID and calculate the co2 emission per day
 * 
 */
const CO2Cal = async function (req, res) {

  console.log("Inside CO2")
    try{
      
        const {UUID} = req.params
        const {day, week, month, year} = req.query
    
        console.log(UUID)
        console.log(day, week, month, year)

        const pipelineDay = [
          {
            $match: { 
                $and:[
                  // {
                  //   TimeStamp: {
                  //     $lte:new Date("2023-12-31")
                  //   },
                  // },
                  {
                    TimeStamp:   { 
                      $gte:new Date("2023-01-01")
                    }
                  },
                  {
                    UUID: {
                     $eq: UUID,
                    }
                  }, 
               
                ]
              } , 
          },
          {
            $addFields:{
              NitrogenKgHec:{$multiply:['$SoilNitrogen', 14]},
              PhosphorousKgHec:{$multiply:['$SoilPhosphorous', 14]},
              PotassiumKgHec:{$multiply:['$SoilPotassium', 14]},
              NCo2:{$multiply:['$SoilNitrogen', 14, 0.00257]},
              PCo2:{$multiply:['$SoilPhosphorous', 14, 0.0014]},
              KCo2:{$multiply:['$SoilPotassium', 14, 0.0011]},
              Co2:{$add:[{$multiply:['$SoilNitrogen', 14, 0.00257]},{$multiply:['$SoilPhosphorous', 14, 0.0014]},{$multiply:['$SoilPotassium', 14, 0.0011]}]},
             
             
              // overCo2:{$add:[{$add:[{$multiply:['$SoilNitrogen', 14, 0.00257]},
              // {$multiply:['$SoilPhosphorous', 14, 0.0014]},
              // {$multiply:['$SoilPotassium', 14, 0.0011]}]
              // }, 14.05509]}
              overCo2:{$add:[{$multiply:['$SoilNitrogen', 14, 0.00257]},
              {$multiply:['$SoilPhosphorous', 14, 0.0014]},
              {$multiply:['$SoilPotassium', 14, 0.0011]}]}
            }
          },
          {    
            $group:{
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, year: { $year: "$TimeStamp" } },
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, month: { $month: { $toDate: '$TimeStamp' } },year: { $year: "$TimeStamp" } },
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$TimeStamp" }},
                co2:{$avg:'$overCo2'},
                NaCo2:{$avg:'$NCo2'},
                PaCo2:{$avg:'$PCo2'},
                KaCo2:{$avg:'$KCo2'},
                // avgNitrogenKgHec: { $sum: '$NitrogenKgHec'},
                // avgPhosphorousKgHec: { $sum: '$PhosphorousKgHec'},
                // avgPotassiumKgHec: { $sum: '$PotassiumKgHec' },

          },
        },
          {
            $addFields:{
                year: { $year: { $toDate: '$_id' } }, 
                month: { $month: { $toDate: '$_id' } }, 
                day: { $dayOfMonth: { $toDate: '$_id' } },
                week:{$week:{$toDate:'$_id'}},
                date: {
                    $dateFromString: {
                      dateString: '$_id',
                    }
                 },
                // x:{ $dateToString: { format: "%Y-%m-%d", date: "$TimeStamp" }}
            }
          },
          {
            $addFields:{
              mon: {
                $let: {
                    vars: {
                        monthsInString: [, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    in: {
                        $arrayElemAt: ['$$monthsInString', '$month']
                    }
                }
            },
            }
          },
          {
            $sort:{
              date:1
            }
        },
        {
          $project:{
            _id:0,
            x:{$concat:[{"$toString":'$day'}, ' ','$mon']},
            co2:1,
            // NaCo2:1,
            // PaCo2:1,
            // KaCo2:1,
          }
        },
    
        ];

        const pipelineMonth = [
          {
            $match: { 
                $and:[
                  // {
                  //   TimeStamp: {
                  //     $lte:new Date("2023-12-31")
                  //   },
                  // },
                  {
                    TimeStamp:   { 
                      $gte:new Date("2023-01-01")
                    }
                  },
                  {
                    UUID: {
                     $eq: UUID,
                    }
                  }, 
               
                ]
              } , 
          },
          {
            $addFields:{
              NitrogenKgHec:{$multiply:['$SoilNitrogen', 14]},
              PhosphorousKgHec:{$multiply:['$SoilPhosphorous', 14]},
              PotassiumKgHec:{$multiply:['$SoilPotassium', 14]},
              NCo2:{$multiply:['$SoilNitrogen', 14, 0.00257]},
              PCo2:{$multiply:['$SoilPhosphorous', 14, 0.0014]},
              KCo2:{$multiply:['$SoilPotassium', 14, 0.0011]},
              Co2:{$add:[{$multiply:['$SoilNitrogen', 14, 0.00257]},{$multiply:['$SoilPhosphorous', 14, 0.0014]},{$multiply:['$SoilPotassium', 14, 0.0011]}]},
              overCo2:{$avg:[{$add:[{$multiply:['$SoilNitrogen', 14, 0.00257]},
              {$multiply:['$SoilPhosphorous', 14, 0.0014]},
              {$multiply:['$SoilPotassium', 14, 0.0011]}]
              }, 14.05509]}
            }
          },
          {    
            $group:{
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, year: { $year: "$TimeStamp" } },
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, month: { $month: { $toDate: '$TimeStamp' } },year: { $year: "$TimeStamp" } },
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$TimeStamp" }},
                CO02:{$avg:'$overCo2'},
          },
        },
          {
            $addFields:{
                year: { $year: { $toDate: '$_id' } }, 
                month: { $month: { $toDate: '$_id' } }, 
                day: { $dayOfMonth: { $toDate: '$_id' } },
                week:{$week:{$toDate:'$_id'}},
                date: {
                    $dateFromString: {
                      dateString: '$_id',
                    }
                 }
            }
          },
        // {
        //     $sort:{
        //         date:1
        //     }
        // },
        {
          $group:{
            // _id:'$month',
            _id:{ $dateToString: { format: "%Y-%m", date: "$date" }},
            co2:{$avg:'$CO02'},
            year:{$first:'$year'},
            month:{$first:'$month'},
          }
        },
        {
          $addFields:{
          // _id:1,
          // CO2:1,
          // y:1,
          // month:1,
          // year:1,
          x: {
            $let: {
                vars: {
                    monthsInString: [, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                in: {
                    $arrayElemAt: ['$$monthsInString', '$month']
                }
            }
        }
    }
        },
        {
          $sort:{
              _id:1
          }
      },
        {
          $project:{
            _id:0,
            x:1,
            co2:1,
          }
        },
      
        ];
      
        const pipelineWeek = [
          {
            $match: { 
                $and:[
                  // {
                  //   TimeStamp: {
                  //     $lte:new Date("2023-12-31")
                  //   },
                  // },
                  {
                    TimeStamp:   { 
                      $gte:new Date("2023-01-01")
                    }
                  },
                  {
                    UUID: {
                     $eq: UUID,
                    }
                  }, 
               
                ]
              } , 
          },
          // {
          //   $match: { 
          //     UUID: UUID, 
          //   }
          // },
          // { 
          //   $filter:{
          //     input:"$TimeStamp",
          //     as: "year",
          //     cond: { $and: [
          //       { $gte: [ "$year",  new Date("2023-01-01T00:00:00.000Z")] },
          //       { $lte: [ "$year",  new Date("2023-12-31T00:00:00.000Z")] }
          //     ] }
          //   }
          // },
          {
            $addFields:{
              NitrogenKgHec:{$multiply:['$SoilNitrogen', 14]},
              PhosphorousKgHec:{$multiply:['$SoilPhosphorous', 14]},
              PotassiumKgHec:{$multiply:['$SoilPotassium', 14]},
              NCo2:{$multiply:['$SoilNitrogen', 14, 0.00257]},
              PCo2:{$multiply:['$SoilPhosphorous', 14, 0.0014]},
              KCo2:{$multiply:['$SoilPotassium', 14, 0.0011]},
              Co2:{$add:[{$multiply:['$SoilNitrogen', 14, 0.00257]},{$multiply:['$SoilPhosphorous', 14, 0.0014]},{$multiply:['$SoilPotassium', 14, 0.0011]}]},
              overCo2:{$avg:[{$add:[{$multiply:['$SoilNitrogen', 14, 0.00257]},
              {$multiply:['$SoilPhosphorous', 14, 0.0014]},
              {$multiply:['$SoilPotassium', 14, 0.0011]}]
              }, 14.05509]}
            }
          },
          {    
            $group:{
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, year: { $year: "$TimeStamp" } },
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, month: { $month: { $toDate: '$TimeStamp' } },year: { $year: "$TimeStamp" } },
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$TimeStamp" }},
                CO02:{$avg:'$overCo2'},
          },
        },
          {
            $addFields:{
                year: { $year: { $toDate: '$_id' } }, 
                month: { $month: { $toDate: '$_id' } }, 
                day: { $dayOfMonth: { $toDate: '$_id' } },
                week:{$week:{$toDate:'$_id'}},
                date: {
                    $dateFromString: {
                      dateString: '$_id',
                    }
                 }
            }
          },
        // {
        //     $sort:{
        //         date:1
        //     }
        // },
        {
          $group:{
            _id:'$week',
            x:{$first:"$week"},
            co2:{$avg:'$CO02'},
            year:{$first:'$year'},
            month:{$first:'$month'},
          }
        },
        {
          $project:{
          _id:1,
          co2:1,
          x:1,
          month:1,
          year:1,
        }
      },
      {
        $project:{
          _id:0,
          x:1,
          co2:1,
        }
      },
      {
        $sort:{
            x:1
        }
  },
        
      
        ];


        let pipleline = [];

        if(day){
          pipleline = pipelineDay;
        }
        else if(month){
          pipleline = pipelineMonth;
        }
        else if(week){
          pipleline = pipelineWeek;
        }
        else{
          pipleline = pipelineDay
        }

        const result = await soilDetailModel.aggregate(pipleline)
        // const result = await soilDetailModel.find({ UUID: UUID });
        return res.status(200).send({status:true, data:result});
      }catch (error) {
        console.log(error)
      return res.status(500).send({ status: false, Error: error.message })
      }
};

module.exports = { CO2Cal };