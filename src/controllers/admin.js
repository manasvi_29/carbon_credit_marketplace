const bcrypt = require("bcrypt");
const validator = require("validator");
const { ObjectId } = require("mongodb");
const strongPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;
const validName = /^[a-zA-Z0-9_]{3,20}$/;
const {
  aadharValidator,
  drivingLiscenseValidator,
  phoneValidator,
} = require("../utils/validator");
const userModel = require("../models/userModel");
const { sendCredentials } = require("../aws/services");
const crypto = require("crypto");
const { uploadFiles } = require("../aws/upload");
const landModel = require("../models/landModel");
const soilDetailModel = require("../models/soilDetailModel");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const { Readable } = require("stream");
const pinataSDK = require("@pinata/sdk");
const pinataConfig = require("../configs/serverConfigs.json").PINATA_CONFIG;
const shortId = require("shortid");
const { co2Emission } = require("./co2Emission");
const batchModel = require("../models/batchModel");
const path = require("path");
const transactionModel = require("../models/transactionModel");
const fs = require("fs");

let admincreated = false;


//create admin as soon as the application starts
const createAdmin = async function () {
  try {
    const adminexist = await userModel.findOne({ role: "admin" });
    if (admincreated == false && !adminexist) {
      let admin = {
        name: "SimplyFi",
        email_id: "simplyfi.123@simplyfi.tech",
        password: "654$!mplyFi",
        active_status: true,
        face_photo: "",
        role: "admin",
      };
      const encryptPassword = await bcrypt.hash(admin.password, 10);
      admin["coded_password"] = encryptPassword;

      const todayDate = new Date();
      const dd = String(todayDate.getDate()).padStart(2, "0");
      const mm = String(todayDate.getMonth() + 1).padStart(2, "0"); //January is 0!e
      const yyyy = todayDate.getFullYear();

      const formattedDate = `${dd}/${mm}/${yyyy}`;

      admin["onboardedDate"] = formattedDate;

      const adminacc = await userModel.create(admin);
      if (adminacc) {
        admincreated = true;
        console.log(adminacc);
      }
    } else {
      admincreated = true;
    }
  } catch (error) {
    console.log(error);
  }
};


const checkUniqueValuesForAuditor = async function (req, res) {
  try {
    let {
      name,
      email_id,
      password,
      national_id,
      mobile_number,
      wallet_address,
      organization
    } = req.body

    let obj = {}
    //check for valid and unique name
    if (!name) {
      return res
        .status(400)
        .send({ status: false, message: "Name is Required" });
    }
    if (!validName.test(name)) {
      return res.status(400).send({
        status: false,
        message:
          "Enter name in correct format, it can only include _,0-9,a-z and no space or special characters",
      });
    }
    let usernameExists = await userModel.findOne({ name: name });
    if (usernameExists) {
      return res
        .status(409)
        .send({ status: false, message: "Username is already taken" });
    }
    obj.name = name

    //check unique and valid emailid
    if (!email_id) {
      return res
        .status(400)
        .send({ status: false, message: "Email is Required" });
    }
    if (!validator.isEmail(email_id)) {
      return res
        .status(400)
        .send({ status: false, message: "Enter email in correct format" });
    }
    email_id = email_id.toLowerCase();
    const isDuplicate = await userModel.findOne({ email_id: email_id })
    if (isDuplicate) {
      return res.status(409).send({
        status: false,
        message: `email - ${email_id} already exists`,
      });
    }
    obj.email_id = email_id

    //check for valid password
    if (!password) {
      return res
        .status(400)
        .send({ status: false, message: "Password is Required" });
    }
    if (!strongPassword.test(password)) {
      return res.status(400).send({
        status: false,
        message:
          "Password should be of minimum 8 and maximum 15 characters also should contain atleast one special character and a number along with aplhabets",
      });
    }
    obj.password = password

    //check for valid and unique nationalid 
    if (!national_id) {
      return res
        .status(400)
        .send({ status: false, message: "National Id is required" });
    }
    if (!aadharValidator(national_id)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid format of Aadhar number" });
    }

    let adhaarExists = await userModel.findOne({ national_id: national_id });
    if (adhaarExists) {
      return res
        .status(409)
        .send({ status: false, message: "Aadhaar number already exists" });
    }
    obj.national_id = national_id

    //check for unique and valid mobile number
    if (!phoneValidator(mobile_number)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid Phone Number" });
    }
    let numberExist = await userModel.findOne({ mobile_number: mobile_number });
    if (numberExist) {
      return res
        .status(409)
        .send({ status: false, message: "Phone number already exists" });
    }
    obj.mobile_number = mobile_number

    //check for unique wallet address
    if (!wallet_address) {
      return res
        .status(400)
        .send({ status: false, message: "Wallet address is required" });
    }
    let walletExist = await userModel.findOne({ wallet_address: wallet_address });
    if (walletExist) {
      return res
        .status(409)
        .send({ status: false, message: "Wallet is occupied" });
    }
    obj.wallet_address = wallet_address

    //check if organization exists
    if (!organization) {
      return res
        .status(400)
        .send({ status: false, message: "Organization name is required" });
    }
    obj.organization = organization

    return res.status(200).send({ status: true, message: "auditor values are valid", data: obj })
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message })
  }
}


const createAuditor = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      console.log(req.token.role);
      return res.status(401).send({
        status: false,
        message: "You're not authorized to perform this task",
      });
    }
    let {
      name,
      email_id,
      password,
      national_id,
      mobile_number,
      wallet_address,
      organization,
    } = req.body;
    let image = req.files;

    let auditor = {};
    if (!name) {
      return res
        .status(400)
        .send({ status: false, message: "Name is Required" });
    }
    if (!validName.test(name)) {
      return res.status(400).send({
        status: false,
        message:
          "Enter name in correct format, it can only include _,0-9,a-z and no space or special characters",
      });
    }
    let usernameExists = await userModel.findOne({ name: name });
    if (usernameExists) {
      return res
        .status(409)
        .send({ status: false, message: "Username is already taken" });
    }
    auditor.name = name;


    if (!email_id) {
      return res
        .status(400)
        .send({ status: false, message: "Email is Required" });
    }
    if (!validator.isEmail(email_id)) {
      return res
        .status(400)
        .send({ status: false, message: "Enter email in correct format" });
    }
    email_id = email_id.toLowerCase();
    const isDuplicate = await userModel.findOne({ email_id: email_id })
    if (isDuplicate) {
      return res.status(409).send({
        status: false,
        message: `email - ${email_id} already exists`,
      });
    }
    auditor.email_id = email_id;

    if (!password) {
      return res
        .status(400)
        .send({ status: false, message: "Password is Required" });
    }
    if (!strongPassword.test(password)) {
      return res.status(400).send({
        status: false,
        message:
          "Password should be of minimum 8 and maximum 15 characters also should contain atleast one special character and a number along with aplhabets",
      });
    }
    auditor.password = password;
    const encryptPassword = await bcrypt.hash(password, 10);
    auditor.coded_password = encryptPassword;

    if (!national_id) {
      return res
        .status(400)
        .send({ status: false, message: "National Id is required" });
    }
    if (!aadharValidator(national_id)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid format of Aadhar number" });
    }

    let adhaarExists = await userModel.findOne({ national_id: national_id });
    if (adhaarExists) {
      return res
        .status(409)
        .send({ status: false, message: "Aadhaar number already exists" });
    }
    auditor.national_id = national_id;

    if (!phoneValidator(mobile_number)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid Phone Number" });
    }
    let numberExist = await userModel.findOne({ mobile_number: mobile_number });
    if (numberExist) {
      return res
        .status(409)
        .send({ status: false, message: "Phone number already exists" });
    }
    auditor.mobile_number = mobile_number;

    if (image.length == 0) {
      return res
        .status(400)
        .send({ status: false, message: "Image is required" });
    }
    let match = /\.(jpg|jpeg|jfif|pjpeg|pjp|webp|png)$/.test(
      image[0].originalname
    );
    if (match == false) {
      return res.status(400).send({
        status: false,
        message:
          "Property Image is required in JPEG/PNG/JPG/JFIF/PJPEG/PJP/WEBP format",
      });
    }
    let uploadedFileURL = await uploadFiles(image[0]);
    console.log(uploadedFileURL);

    if (!wallet_address) {
      return res
        .status(400)
        .send({ status: false, message: "Wallet address is required" });
    }
    let walletExist = await userModel.findOne({ wallet_address: wallet_address });
    if (walletExist) {
      return res
        .status(409)
        .send({ status: false, message: "Wallet is occupied" });
    }

    if (!organization) {
      return res
        .status(400)
        .send({ status: false, message: "Organization name is required" });
    }

    const todayDate = new Date();
    const dd = String(todayDate.getDate()).padStart(2, "0");
    const mm = String(todayDate.getMonth() + 1).padStart(2, "0"); //January is 0!e
    const yyyy = todayDate.getFullYear();

    const formattedDate = `${dd}/${mm}/${yyyy}`;

    auditor.onboardedDate = formattedDate;
    auditor.role = "auditor";
    auditor.wallet_address = wallet_address;
    auditor.active_status = true;
    auditor.face_photo = uploadedFileURL.blobURL;
    auditor.organization = organization;
    console.log(auditor);
    const newAuditor = await userModel.create(auditor);

    if (!newAuditor) {
      return res
        .status(400)
        .send({ status: false, message: "Failed to create Auditor" });
    }
    let obj = {
      name: newAuditor.name,
      emailId: newAuditor.email_id,
      wallet_address: newAuditor.wallet_address,
    };
    //sending credential to auditor
    sendCredentials(auditor)
      .then((result) => {
        console.log(result);
        return res.status(201).send({
          status: true,
          message:
            "Credentials have been sent to your registered mobile number of auditor",
          obj,
        });
      })
      .catch((err) => {
        console.log(err);
        return res
          .status(500)
          .send({ status: false, message: "Internal server error" });
      });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const adminKeys = async function (req, res) {
  try {
    const { private_key, wallet_address } = req.body;
    // Generate a random initialization vector (IV)

    // Generate a random 256-bit key for encryption
    const encryptionKey = crypto.randomBytes(32);

    // Create a cipher object using AES-256-CBC algorithm and the encryption key
    const cipher = crypto.createCipheriv(
      "aes-256-cbc",
      encryptionKey,
      crypto.randomBytes(16)
    );

    // Encrypt the private key using the cipher object
    let encryptedPrivateKey = cipher.update(private_key, "utf8", "base64");
    encryptedPrivateKey += cipher.final("base64");

    // Store the encrypted private key and encryption key somewhere safe
    console.log("Encrypted private key:", encryptedPrivateKey);
    console.log("Encryption key:", encryptionKey.toString("base64"));

    const updatedAdmin = await userModel.findOneAndUpdate(
      { _id: req.token.user_id, role: "admin" },
      {
        $set: {
          encryption_key: encryptionKey.toString("base64"),
          private_key: encryptedPrivateKey,
          wallet_address: wallet_address,
        },
      },
      { new: true }
    );

    if (!updatedAdmin) {
      return res
        .status(404)
        .send({ status: false, message: "admin not found" });
    }
    return res.status(200).send({
      status: true,
      message: "address added to admin data",
      data: updatedAdmin,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const adminDashboard = async function (req, res) {
  try {
    const batches = await batchModel.find({ status: "Approved" });

    let obj = {
      total_audited_users: batches.length,
      tokens: 0,
      reduced_carbon: 0,
    };

    batches.map((x) => {
      if (x.carbon_credits) obj.tokens += x.carbon_credits;
      obj.reduced_carbon += x.co2TotalReduction;
    });

    const users = await userModel.find();
    let marketplace_analysis = {
      onboarding: users.length,
      batch_audit: batches.length,
      issuance: 0,
    };
    batches.map((x) => {
      if (x.carbon_credits) marketplace_analysis.issuance += 1;
    });

    let userOverview = {
      Approved: 0,
      Pending: 0,
      Rejected: 0,
    };
    //user overview yet to be done
    const batchesData = await batchModel.find();
    if (batchesData.length > 0) {
      batchesData.map((x) => {
        if (x.status == "Approved") {
          userOverview.Approved += 1;
        } else if (x.status === "Rejected") {
          userOverview.Rejected += 1;
        } else if (x.status === "Pending") {
          userOverview.Pending += 1;
        }
      });
    }



    const topCredit = await userModel.find({ role: 'farmer' }).sort({ carbon_credits: -1 })
    return res.status(200).send({
      status: true,
      message: "admin dashboard",
      obj,
      marketplace_analysis,
      userOverview,
      topCredit,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const allProject = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(401).send({
        status: true,
        message: "You're not authorized to access this",
      });
    }
    const { farmer_id } = req.params;

    const projects = await landModel.find({ farmer: farmer_id });

    if (projects.length == 0) {
      return res
        .status(404)
        .send({ status: false, message: "There is no project by this farmer" });
    }

    return res
      .status(200)
      .send({ status: true, message: "Project List", projects });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const auditorDetails = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(401).send({
        status: true,
        message: "You're not authorized to access this",
      });
    }

    const auditors = await userModel.find({ role: "auditor" });
    if (auditors.length == 0) {
      return res
        .status(404)
        .send({ status: false, message: "No auditor to show" });
    }

    return res
      .status(200)
      .send({ status: true, message: "auditor list", auditors });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const farmerDetails = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(401).send({
        status: true,
        message: "You're not authorized to access this",
      });
    }

    const farmers = await userModel.find({ role: "farmer" });
    if (farmers.length == 0) {
      return res
        .status(404)
        .send({ status: false, message: "No farmer to show" });
    }

    return res
      .status(200)
      .send({ status: true, message: "farmer list", farmers });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const totalCount = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(401).send({
        status: true,
        message: "You're not authorized to access this",
      });
    }

    let users = await userModel.find({ role: { $ne: "admin" } });
    let obj = {
      total_auditors: 0,
      total_farmers: 0,
      total_users: users.length,
    };

    users.map((x) => {
      if (x.role === "auditor") obj.total_auditors += 1;
      else if (x.role === "farmer") obj.total_farmers += 1;
    });

    if (users.length == 0) {
      obj.total_auditors = 0;
      obj.total_farmers = 0;
      obj.total_users = 0;
    }

    return res
      .status(200)
      .send({ status: true, message: "counts of users", obj });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const blockUser = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(401).send({
        status: true,
        message: "You're not authorized to access this",
      });
    }

    const { block_id } = req.params;

    const user = await userModel.findOneAndUpdate(
      { _id: block_id },
      { $set: { blocked: true, active_status: false } },
      { new: true }
    );

    if (!user) {
      return res.status(404).send({ status: false, message: "user not found" });
    }

    return res
      .status(200)
      .send({ status: true, message: "user blocked successfully", user });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const unblockUser = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(401).send({
        status: true,
        message: "You're not authorized to access this",
      });
    }

    const { block_id } = req.params;

    const user = await userModel.findOneAndUpdate(
      { _id: block_id },
      { $set: { blocked: false, active_status: true } },
      { new: true }
    );

    if (!user) {
      return res.status(404).send({ status: false, message: "user not found" });
    }

    return res
      .status(200)
      .send({ status: true, message: "user unblocked successfully", user });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const filterProjects = async function (req, res) {
  try {
    const { crop } = req.query;
    let projectList = await landModel.find({ crops_grown: crop, set_in_batch: false }).populate({
      path: 'farmer',
      select: { _id: 1, name: 1, wallet_address: 1 }
    })


    const projectsOfFarmers = projectList.map(project => ({
      _id: project.farmer._id,
      name: project.farmer.name,
      wallet_address: project.farmer.wallet_address,
      projects: {
        _id: project._id,
        farmer: project.farmer._id,
        location: project.location,
        land_size: project.land_size,
        imei_no: project.imei_no,
        baseLine_co2_emission_rate: project.baseLine_co2_emission_rate,
        cO2emitted: project.cO2emitted,
        added_on: project.added_on,
        status: project.status,
        crops_grown: project.crops_grown,
        set_in_batch: project.set_in_batch,
        co2_reduction_diff: project.co2_reduction_diff,
        start_time: project.start_time,
        end_time: project.end_time,
        end_time_reached: project.end_time_reached,
        ipfs_hash: project.ipfs_hash
      }
    }));


    if (projectList.length == 0) {
      return res.status(404).send({
        status: false,
        message: "No Land Project found with this crop",
      });
    }

    return res
      .status(200)
      .send({ status: true, message: "List of Land Projects", projectsOfFarmers });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const createBatchOfProjects = async function (req, res) {
  try {
    // console.log(path.join(__dirname));
    //send batch of projects in an array
    const { projects_arr } = req.body;
    let sensorDataArr = [];

    for (project of projects_arr) {
      let sensorData = await soilDetailModel
        .find({ UUID: project.imei_no })
        .sort({ createdAt: -1 })
        .limit(1);
      sensorDataArr.push(sensorData[0]);
    }

    let date = Date.now();
    let name = projects_arr[0].crops_grown + date;
    const csvWriter = createCsvWriter({
      path: `${path.join(__dirname)}/../sensorDataFiles/${name}.csv`,
      header: [
        { id: "deviceName", title: "deviceName" },
        { id: "sensorName", title: "sensorName" },
        { id: "UUID", title: "UUID" },
        { id: "TimeStamp", title: "TimeStamp" },
        { id: "Date", title: "Date" },
        { id: "CO2", title: "CO2" },
        { id: "SoilTemperature", title: "SoilTemperature" },
        { id: "SoilNitrogen", title: "SoilNitrogen" },
        { id: "SoilPhosphorous", title: "SoilPhosphorous" },
        { id: "SoilPotassium", title: "SoilPotassium" },
        { id: "SoilPH", title: "SoilPH" },
      ],
    });
    // console.log(sensorDataArr)
    await csvWriter.writeRecords(sensorDataArr);

    const filePath = `${path.join(__dirname)}/../sensorDataFiles/${name}.csv`;
    // Read the CSV file into a buffer
    const buffer = fs.readFileSync(filePath);

    console.log(buffer);

    const stream = Readable.from(buffer);

    // return res.send("done")
    //now upload the csv file on pinata and return ipfs hash
    let options = {
      pinataMetadata: {
        name: name.toString(),
      },
    };
    const pinata = new pinataSDK(
      `${pinataConfig.apiKey}`,
      `${pinataConfig.apiSecret}`
    );
    let result = await pinata.pinFileToIPFS(stream, options);
    console.log(result);
    if (result) {
      let finalData = [];
      let farmers = [];
      let co2emission = 0;
      let co2BaselineTotal = 0;
      for (project of projects_arr) {
        const updatedProjects = await landModel.findOneAndUpdate(
          { imei_no: project.imei_no },
          { $set: { ipfs_hash: result.IpfsHash, set_in_batch: true } },
          { new: true }
        );
        console.log(updatedProjects);
        if (!farmers.includes(updatedProjects.farmer.toString())) {
          farmers.push(updatedProjects.farmer.toString());
        }
        co2emission += updatedProjects.final_cO2;
        co2BaselineTotal += updatedProjects.baseLine_co2_emission_rate;
        finalData.push(updatedProjects);
      }

      const batch = {};
      batch["batch_id"] = shortId.generate();
      batch["crop"] = projects_arr[0].crops_grown;
      batch["farmers"] = farmers.length;
      batch["farmers_arr"] = farmers;
      batch["batch_ipfs_hash"] = result.IpfsHash;
      batch["co2emission"] = co2emission;
      batch["co2BaselineTotal"] = co2BaselineTotal;
      // console.log(batch.co2BaselineTotal - batch.co2emission);
      if (co2emission === 0) {
        batch["co2TotalReduction"] = 0;
      } else
        batch["co2TotalReduction"] = batch.co2BaselineTotal - batch.co2emission;

      const newBatch = await batchModel.create(batch);

      //create batch data
      return res.status(200).send({
        status: true,
        message: "uploaded files of projects",
        finalData,
        newBatch,
      });
    } else {
      return res
        .status(400)
        .send({ status: false, message: "Something went wrong !!" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const updateBatch = async function (req, res) {
  try {
    const { user_id, batch_id } = req.params;
    if (req.token.role !== "admin") {
      return res.status(401).send({
        status: false,
        message: "You are not authorized to proceed further",
      });
    }
    const batch = await batchModel.findOne({ batch_id: batch_id });
    const {
      startTime,
      endTime,
      batch_project_id,
      batch_claim_id,
      co2BaselineTotal,
    } = req.body;
    let obj = {};

    if (!startTime) {
      return res
        .status(400)
        .send({ status: false, message: "startTime is required" });
    }
    obj["startTime"] = startTime;

    if (!endTime) {
      return res
        .status(400)
        .send({ status: false, message: "endTime is required" });
    }
    obj["endTime"] = endTime;

    if (!batch_project_id) {
      return res
        .status(400)
        .send({ status: false, message: "batch project Id is required" });
    }
    obj["batch_project_id"] = batch_project_id;

    if (!batch_claim_id) {
      return res
        .status(400)
        .send({ status: false, message: "batch claim Id is required" });
    }
    obj["batch_claim_id"] = batch_claim_id;

    if (co2BaselineTotal) {
      obj["co2BaselineTotal"] = co2BaselineTotal;
      if (batch.co2emission == 0) {
        obj["co2TotalReduction"] = 0;
      } else obj["co2TotalReduction"] = co2BaselineTotal - batch.co2emission;
    }
    const updatedBatch = await batchModel.findOneAndUpdate(
      { batch_id: batch_id },
      { $set: obj },
      { new: true }
    );

    if (!updatedBatch) {
      return res
        .status(404)
        .send({ status: false, message: "Batch not found" });
    }

    return res.status(200).send({
      status: true,
      message: "Batch Updated Successfully",
      updatedBatch,
    });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const getAllBatch = async function (req, res) {
  try {
    // if(req.token.role !== "admin"){
    //   return res.status(400).send({status:false, message:"You're not authorized to proceed further"})
    // }

    const allBatches = await batchModel.find();
    if (allBatches.length == 0) {
      return res
        .status(404)
        .send({ status: false, message: "No batches found" });
    }

    return res
      .status(200)
      .send({ status: true, message: "Batches list", allBatches });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const getFarmersFromBatch = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(400).send({
        status: false,
        message: "You're not authorized to proceed further",
      });
    }

    const { batch_id } = req.params;

    const batch = await batchModel.findOne({ batch_id: batch_id });

    if (!batch) {
      return res
        .status(404)
        .send({ status: false, message: "Batch not found" });
    }
    let arr = batch.farmers_arr



    // Retrieve farmer data
    const farmers = await userModel.find({ _id: { $in: arr } });

    // Retrieve soil details for each farmer
    const farmerData = [];
    for (let farmer of farmers) {
      const lands = await landModel.find({ farmer: farmer._id });
      const latestSoilDetail = await soilDetailModel
        .find({ UUID: { $in: lands.map((land) => land.imei_no) } })
        .sort({ createdAt: -1 })
        .limit(1)
        .lean();

      farmerData.push({
        _id: farmer._id,
        name: farmer.name,
        active_status: farmer.active_status,
        DOB: farmer.DOB,
        mobile_number: farmer.mobile_number,
        address: farmer.address,
        role: farmer.role,
        email_id: farmer.email_id,
        payment_status: farmer.payment_status,
        wallet_address: farmer.wallet_address,
        onboardedDate: farmer.onboardedDate,
        co2_diff: lands[0]?.co2_reduction_diff,
        soil_detail: latestSoilDetail[0] || null,
      });
    }
    return res
      .status(200)
      .send({ status: true, message: "farmer list", farmerData });
  } catch (error) {
    console.log(error)
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const filterBatchByStatus = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(400).send({
        status: false,
        message: "You're not authorized to proceed further",
      });
    }

    const { status } = req.query;

    if (!["Approved", "Rejected", "New"].includes(status)) {
      return res.status(400).send({
        status: false,
        message: "Enter valid status--- Approved, Rejected or New",
      });
    }

    const allBatch = await batchModel.find({ status: status });

    if (allBatch.length == 0) {
      return res.status(404).send({ status: false, message: "No Batch Found" });
    }

    return res
      .status(200)
      .send({ status: true, message: "Batch list", allBatch });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const changeBatchToPending = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(400).send({
        status: false,
        message: "You're not authorized to proceed further",
      });
    }

    const { batch_id } = req.params;

    const batch = await batchModel.findOneAndUpdate(
      { batch_id: batch_id },
      { $set: { status: "Pending" } },
      { new: true }
    );

    if (!batch) {
      return res
        .status(404)
        .send({ status: false, message: "Batch not found" });
    }
    return res
      .status(200)
      .send({ status: true, message: "Batch sent for auditing", batch });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const approvedbatchData = async function (req, res) {
  try {
    // if(req.token.role !== "admin"){
    //   return res.status(400).send({status:false, message:"You're not authorized to proceed further"})
    // }

    const allBatches = await batchModel.find({ status: "Approved" });
    if (allBatches.length == 0) {
      return res
        .status(404)
        .send({ status: false, message: "No batches found" });
    }

    return res
      .status(200)
      .send({ status: true, message: "Batches list", allBatches });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const transactionHistory = async function (req, res) {
  try {
    if (req.token.role !== "admin") {
      return res.status(400).send({
        status: false,
        message: "You're not authorized to proceed further",
      });
    }
    const {
      farmer_name,
      wallet_address,
      carbon_credits,
      payment_completed,
      txn_hash,
    } = req.body;

    if (!farmer_name) {
      return res
        .status(400)
        .send({ status: false, message: "farmer name is required" });
    }

    if (!wallet_address) {
      return res
        .status(400)
        .send({ status: false, message: "wallet address is required" });
    }
    const farmerData = await userModel.findOne({
      wallet_address: wallet_address,
      name: farmer_name,
    });

    if (!farmerData) {
      return res
        .status(404)
        .send({ status: false, message: "Farmer not found" });
    }

    if (!carbon_credits) {
      return res.status(400).send({
        status: false,
        message: "Number of carbon credits are required",
      });
    }

    if (!payment_completed) {
      return res
        .status(400)
        .send({ status: false, message: "payment status is required" });
    }

    if (!txn_hash) {
      return res
        .status(400)
        .send({ status: false, message: "txn hash is required" });
    }

    const obj = {
      farmer: farmerData._id,
      wallet_address: wallet_address,
      no_of_carbonCredit: carbon_credits,
      isPaymentCompleted: payment_completed,
      txn_hash: txn_hash,
    };

    const transactionData = await transactionModel.create(obj);

    console.log(carbon_credits)

    let newfarmer = await userModel.findOneAndUpdate({ _id: farmerData._id }, { $set: { payment_status: true }, $inc: { carbon_credits: carbon_credits } }, { new: true })
    return res
      .status(200)
      .send({ status: true, message: "Transaction done", transactionData, newfarmer });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const getTransactionHistory = async function (req, res) {
  try {
    const { wallet_address } = req.params;

    const transactionDetails = await transactionModel.find({
      wallet_address: wallet_address,
    });

    if (transactionHistory.length == 0) {
      return res
        .status(404)
        .send({ status: false, message: "No transaction yet" });
    }

    return res.status(200).send({
      status: true,
      message: "Transaction History",
      transactionDetails,
    });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

module.exports = {
  createAdmin,
  checkUniqueValuesForAuditor,
  createAuditor,
  adminKeys,
  adminDashboard,
  allProject,
  auditorDetails,
  farmerDetails,
  totalCount,
  blockUser,
  unblockUser,
  filterProjects,
  createBatchOfProjects,
  updateBatch,
  getAllBatch,
  getFarmersFromBatch,
  filterBatchByStatus,
  changeBatchToPending,
  approvedbatchData,
  transactionHistory,
  getTransactionHistory,
};
