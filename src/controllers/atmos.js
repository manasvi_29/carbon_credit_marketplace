const soilDetailModel = require("../models/soilDetailModel");


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns Data and state of the api
 * @function From mongo obtain the data with UUID and calculate the co2 emission per day
 * 
 */
const atmos = async function (req, res) {


  /**
   * How Query is Done
   * --> Filter the entire data based on year and UUID provided Note** for now the start and end is 1st and last day of the day for year
   * --> Converting NPK to Kg/Hec (*14).
   * --> Taking Day's Summation by Group Aggregate.
   * --> Adding Fields for Year, week, month based representation for further grouping. 
   */

    try{
      
        const {UUID} = req.params
        const {day, week, month, year} = req.query
    
        console.log(UUID)
        console.log(day, week, month, year)

        const pipelineDay = [
          {
            $match: { 
                $and:[
                  // {
                  //   TimeStamp: {
                  //     $lte:new Date("2023-12-31")
                  //   },
                  // },
                  {
                    TimeStamp:   { 
                      $gte:new Date("2023-01-01")
                    }
                  },
                  {
                    UUID: {
                     $eq: UUID,
                    }
                  }, 
               
                ]
              } , 
          },
          {    
            $group:{
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$TimeStamp" }},
                "Co2":{$avg:'$CO2'}
              },
        },
          {
            $addFields:{
                year: { $year: { $toDate: '$_id' } }, 
                month: { $month: { $toDate: '$_id' } }, 
                day: { $dayOfMonth: { $toDate: '$_id' } },
                week:{$week:{$toDate:'$_id'}},
                date: {
                    $dateFromString: {
                      dateString: '$_id',
                    }
                 }
            }
          },
          {
            $addFields:{
              mon: {
                $let: {
                    vars: {
                        monthsInString: [, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    in: {
                        $arrayElemAt: ['$$monthsInString', '$month']
                    }
                }
            },
            }
          },
          {
            $sort:{
                date:1
            }
        }, 
          {
              $project:{
                _id:0,
                x:{$concat:[{"$toString":'$day'}, ' ','$mon']},
                "Co2":1,
              }
          },     
        ];

        const pipelineMonth = [
          {
            $match: { 
                $and:[
                  // {
                  //   TimeStamp: {
                  //     $lte:new Date("2023-12-31")
                  //   },
                  // },
                  {
                    TimeStamp:   { 
                      $gte:new Date("2023-01-01")
                    }
                  },
                  {
                    UUID: {
                     $eq: UUID,
                    }
                  }, 
               
                ]
              } , 
          },
          {    
            $group:{
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$TimeStamp" }},
                // avgNitrogenKgHec: { $avg: '$NitrogenKgHec'},
                // avgPhosphorousKgHec: { $avg: '$PhosphorousKgHec'},
                // avgPotassiumKgHec: { $avg: '$PotassiumKgHec' },
                dayCo2:{'$avg':'$CO2'}
          },
        },
          {
            $addFields:{
                year: { $year: { $toDate: '$_id' } }, 
                month: { $month: { $toDate: '$_id' } }, 
                day: { $dayOfMonth: { $toDate: '$_id' } },
                week:{$week:{$toDate:'$_id'}},
                date: {
                    $dateFromString: {
                      dateString: '$_id',
                    }
                 }
            }
          },
        {
          $group:{
            // _id:'$month',
            _id:{ $dateToString: { format: "%Y-%m", date: "$date" }},
            "Co2":{$avg:'$dayCo2'},
            year:{$first:'$year'},
            month:{$first:'$month'},
          }
        },
        {
          $addFields:{
          x: {
            $let: {
                vars: {
                    monthsInString: [, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                in: {
                    $arrayElemAt: ['$$monthsInString', '$month']
                }
            }
        }
    }
    },
        {
            $sort:{
                _id:1
            }
        },
        {
          $project:{
            _id:0,
            "Co2":1,
            x:1,
          }
        }
      
        ];
      
        const pipelineWeek = [
          {
            $match: { 
                $and:[
                  // {
                  //   TimeStamp: {
                  //     $lte:new Date("2023-12-31")
                  //   },
                  // },
                  {
                    TimeStamp:   { 
                      $gte:new Date("2023-01-01")
                    }
                  },
                  {
                    UUID: {
                     $eq: UUID,
                    }
                  }, 
               
                ]
              } , 
          },
          {    
            $group:{
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, year: { $year: "$TimeStamp" } },
                // _id: { day: { $dayOfMonth: "$TimeStamp"}, month: { $month: { $toDate: '$TimeStamp' } },year: { $year: "$TimeStamp" } },
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$TimeStamp" }},
                dayCo2: { $avg: '$CO2'},
          },
        },
          {
            $addFields:{
                year: { $year: { $toDate: '$_id' } }, 
                month: { $month: { $toDate: '$_id' } }, 
                day: { $dayOfMonth: { $toDate: '$_id' } },
                week:{$week:{$toDate:'$_id'}},
                date: {
                    $dateFromString: {
                      dateString: '$_id',
                    }
                 }
            }
          },
        {
          $group:{
            _id:'$week',
            x:{$first:"$week"},
            "Co2":{$avg:'$dayCo2'},
            year:{$first:'$year'},
            month:{$first:'$month'},
          }
        },
      //   {
      //     $project:{
      //     _id:1,
      //     nitrogen:1,
      //     phosphorous:1,
      //     potassium:1,
      //     week:1,
      //     month:1,
      //     year:1,
      //   }
      // },
      {
            $sort:{
                x:1
            }
      },
      {
        $project:{
          _id:0,
          "Co2":1,
          x:1
        }
      },];
      
        let pipleline = [];

        if(day){
          pipleline = pipelineDay;
        }
        else if(month){
          pipleline = pipelineMonth;
        }
        else if(week){
          pipleline = pipelineWeek;
        }
        else{
          pipleline = pipelineDay
        }

        const result = await soilDetailModel.aggregate(pipleline)
        // const result = await soilDetailModel.find({ UUID: UUID });
        return res.status(200).send({status:true, data:result});
      }catch (error) {
        console.log(error)
      return res.status(500).send({ status: false, Error: error.message })
      }
};

module.exports = { atmos };