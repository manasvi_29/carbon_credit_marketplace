const { uploadFiles, downloadFile } = require("../aws/upload");
const auditModel = require("../models/auditModel");
const batchModel = require("../models/batchModel");
const projectModel = require("../models/landModel")
const pinataConfig = require("../configs/serverConfigs.json").PINATA_CONFIG;
const pinataSDK = require("@pinata/sdk");
const { Readable } = require("stream");
const landModel = require("../models/landModel");



const getParticularBatch = async function(req,res){
    try{
          const{batch_id} = req.params

          const batch = await batchModel.findOne({batch_id:batch_id})

          if(!batch){
            return res.status(404).send({status:false, message:"Batch not found"})
          }

          return res.status(200).send({status:true, message:"Batch Details",batch})
    }catch(error){
      return res.status(500).send({status:false, Error:error.message})
    }
}

const statusChange = async function(req,res){
    try{
        const {batch_id} = req.params
        const {status} = req.body
        const user_id = req.token.user_id

        let file = req.files
        console.log(file[0])
        // return res.send("done")
        if(file.length == 0){
          return res.status(400).send({status:false,message:"Please upload necessary document"})
        }
        const checkBatch = await batchModel.findOne({batch_id:batch_id})

        if(!checkBatch){
          return res.status(404).send({status:false,message:"project not found"})
        }

        let obj = {}
        if (!status) {
          return res
            .status(400)
            .send({ status: false, message: "Status is required" });
        }
        if (!["Approved", "Rejected"].includes(status)) {
          return res.status(400).send({
            status: false,
            message: "Status can only be approved or rejected",
          });
        }
        
        if(checkBatch.status === "Approved"){
          return res.status(400).send({status:false,message:"Batch already been approved"})
        }
        let batch
        if(checkBatch.co2TotalReduction < 0 && status == "Approved"){
          return res.status(400).send({status:false,message:"This batch have emission higher than baseline emission and hence can not be approved"})
        }else if(checkBatch.co2TotalReduction < 0 && status === "Rejected"){
          batch = await batchModel.findOneAndUpdate({batch_id:batch_id},{$set:{status:status,audited_by:user_id}},{new:true})
        }else if(checkBatch.co2TotalReduction >= 0){
          batch = await batchModel.findOneAndUpdate({batch_id:batch_id},{$set:{status:status,audited_by:user_id}},{new:true})
        }

      obj.batch = batch_id
      obj.audited_by = user_id
      obj.status = status
      obj.blobName = file[0].originalname.split(".")[0] +
      Date.now() +
      "." +
      file[0].mimetype.split("/")[1]
    let uploadedFileURL = await uploadFiles(file[0]);
    console.log(uploadedFileURL.blobURL)

    obj.document = uploadedFileURL.blobURL
    

    const stream = Readable.from(file[0].buffer);
    let options = {
      pinataMetadata: {
        name: file[0].originalname,
      },
    };
    const pinata = new pinataSDK(
      `${pinataConfig.apiKey}`,
      `${pinataConfig.apiSecret}`
    );
    let result = await pinata.pinFileToIPFS(stream, options);
    console.log(result);
    if (result) {
      obj.ipfs_hash = result.IpfsHash;
    }

    let audited = await auditModel.create(obj)

    //also generate the hash of the document
    return res.status(200).send({ status: true, message:"Batch audited successfully", audited, batch});
    }catch(error){
      console.log(error)
        return res.status(500).send({status:false,Error:error.message})
    }
}


const auditClaim = async function(req,res){
  try{
    //do this for batches
    const {greennftAddress, auditorAddress, ownerBalance, auditedReport, claimId, farmerAddress, issuedDate, greenNftStatus, carbonCredits, buyableCarbonCredits, nftId} = req.body
    const {user_id, batch_id} = req.params

    let obj = {
      green_nft_address: greennftAddress,
      auditor_address: auditorAddress,
      owner_balance: parseInt(ownerBalance)/(10**18),
      audited_report: auditedReport,
      claim_id: parseInt(claimId),
      farmer_address: farmerAddress,
      issued_date: parseInt(issuedDate),
      green_nft_status: greenNftStatus,
      carbon_credits: parseInt(carbonCredits)/(10**18),
      buyable_carbon_credits: parseInt(buyableCarbonCredits)/(10**18),
      nftId:parseInt(nftId)
    }

    const updatedBatch = await batchModel.findOneAndUpdate({batch_id:batch_id},{$set:obj},{new:true})
    if(!updatedBatch){
      return res.status(404).send({status:false, message:"batch not found"})
    }

    return res.status(200).send({status:true,message:"audit claim report added",updatedBatch})
  }catch(error){
      console.log(error)
        return res.status(500).send({status:false,Error:error.message})
    }
}


const getCLaimData = async function(req,res){
  try{
    //do this for batches
      const{batch_id} = req.params
      const batch = await batchModel.findOne({batch_id:batch_id})

      if(!batch){
        return res.status(404).send({status:false, message:"Batch not found"})
      }
      return res.status(200).send({status:true,message:"Audit claim Data",batch})
  }catch(error){
    console.log(error)
    return res.status(500).send({status:false, Error:error.message})
  }
}

module.exports = {statusChange, auditClaim, getCLaimData, getParticularBatch}