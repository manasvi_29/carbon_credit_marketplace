const projectModel = require("../models/landModel");
const mongoose = require("mongoose");
const userModel = require("../models/userModel");
const ObjectId = mongoose.Types.ObjectId;


const getAllProjects = async function (req, res) {
  try {
    const user_id = req.token.user_id;
 

    const pipeline =[
      {
        $match:{
          role: { $ne: 'admin' } 
        }
      },
      {
        $lookup: {
          from: "lands",
          localField: "_id",
          foreignField: "farmer",
          as: "projects",
        },
      },
      {
        $unwind:'$projects'
      },
      {
        $match:{
          "projects.set_in_batch": false
        }
      },
      {
        $project: {
          wallet_address:1.0,
          name:1.0,
          projects:1.0
        },
      }
    ]
    
    const projectsOfFarmers = await userModel.aggregate(pipeline)
    if (projectsOfFarmers.length == 0) {
      return res
        .status(404)
        .send({ status: false, message: "no projects found" });
    }

    return res.status(200).send({ status: true, projectsOfFarmers });
  } catch (error) {
    console.log(error)
    return res.status(500).send({ status: false, Error: error.message });
  }
};


const getParticularProject = async function (req, res) {
  try {
    let { project_id } = req.params;
    let pipeline = [
      {
        $lookup: {
          from: "users",
          localField: "farmer",
          foreignField: "_id",
          as: "farmer",
        },
      },
      {
        $lookup: {
          from: "claims",
          localField: "_id",
          foreignField: "project",
          as: "claim_doc"
        }
      },
      {
        $match: {
          _id: new ObjectId(project_id),
        },
      },
      {
        $project: {
          _id: 0.0,
          projectId: 1.0,
          project_name: 1.0,
          project_image: 1.0,
          project_description: 1.0,
          average_co2_emission_rate: 1.0,
          cO2emitted:1.0,
          co2_reduction_diff:1.0,
          land_size: 1.0,
          start_time: 1.0,
          end_time: 1.0,
          "farmer.name": 1.0,
          "claim_doc":1.0
        },
      },
    ];

    const details = await projectModel.aggregate(pipeline);

    if (details.length == 0) {
      return res.status(404).send({ status: false, message: "No data found" });
    }

    return res.status(200).send({ status: true, details });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const searchProject = async function (req, res) {
  try {
    const { projectName, user_name } = req.query;

    if (projectName) {
      let obj = {
        project_name: { $regex: projectName },
      };
      const projects = await projectModel.find(obj);
      if (projects.length == 0) {
        return res
          .status(404)
          .send({ status: false, message: "No such project exists" });
      }
      return res.status(200).send({ status: true, projects });
    }
    let pipeline;
    if (user_name) {
      pipeline = [
        {
          $match: {
            name: user_name,
          },
        },
        {
          $lookup: {
            from: "projects",
            localField: "_id",
            foreignField: "farmer",
            as: "projects",
          },
        },
        {
          $project: {
            projects: 1.0,
          },
        },
      ];

      const project_details = await userModel.aggregate(pipeline);
      if (project_details.length == 0) {
        return res
          .status(404)
          .send({ status: false, message: "no project for this farmer" });
      }
      return res.status(200).send({ status: true, project_details });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};
module.exports = { getParticularProject, searchProject, getAllProjects };
