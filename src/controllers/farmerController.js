const userModel = require("../models/userModel");
const axios = require("axios");
const validator = require("validator");
const mongoose = require("mongoose");
const karzaConfig = require("../configs/karzaConfigs.json");
const landModel = require("../models/landModel");
const objectId = mongoose.Types.ObjectId;
const moment = require("moment");
const {
  provider,
  CarbonCreditTokenABI,
  carbonCreditTokenaddress,
} = require("../abis/connector");
const jwt = require("jsonwebtoken");
const { phoneValidator, aadharValidator } = require("../utils/validator");
const randomString = require("randomstring");
const { JWT } = require("../configs/generalConfigs.json");
const {
  cronCreateSensorData,
  cancelScheduledJobs,
} = require("../utils/cronJob");
const { ethers } = require("ethers");
const {
  sendOTP,
  forgotPasswordOTP,
  resetPasswordMsg,
} = require("../aws/services");
const { uploadFiles } = require("../aws/upload");

const registerWithNationaId = async function (req, res) {
  try {
    const { nationalId } = req.body;

    if(!nationalId){
      return res.status(400).send({status:false, message:"National Id is required"})
    }
    if(!aadharValidator(nationalId)){
      return res.status(400).send({status:false, message:"Enter Valid Adhaar Number"})
    }

    const adhaarExist = await userModel.findOne({ national_id: nationalId });
    if (adhaarExist) {
      return res
        .status(409)
        .send({ status: false, message: "This adhaar already exists" });
    }
    
    // const config = {
    //   url: "https://testapi.karza.in/v3/aadhaar-xml/otp",
    //   method: "POST",
    //   headers: {
    //     "x-karza-key": `${karzaConfig.API_KEY}`,
    //     "Content-Type": "application/json",
    //   },
    //   data: {
    //     aadhaarNo: nationalId,
    //     consent: "Y",
    //   },
    // };

    // let response = await axios(config);
    // return res.status(200).send({ status: true, response: response.data });
    // return res.status(200).send({ status: true, response: { requestId: "70314811-3da6-4cc3-b92e-eb17283088ac",
    // result: {
    //     "message": "OTP sent to registered mobile number"
    // },
    // statusCode: 101}});
     return res.status(200).send({ status: true, message:"Adhar number Validated Successfully", data:nationalId});
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const verifyNationalId = async function (req, res) {
  try {
    const { otp, nationalId, requestId } = req.body;

    if(!nationalId){
      return res.status(400).send({status:false, message:"National Id is required"})
    }
    if(!aadharValidator(nationalId)){
      return res.status(400).send({status:false, message:"Enter Valid Adhaar Number"})
    }

    if(!otp){
      return res.status(400).send({status:false, message:"OTP is required"})
    }

    if(!requestId){
      return res.status(400).send({status:false, message:"RequestId is required"})
    }

    // const config = {
    //   url: "https://testapi.karza.in/v3/aadhaar-xml/file",
    //   method: "POST",
    //   headers: {
    //     "x-karza-key": `${karzaConfig.API_KEY}`,
    //     "Content-Type": "application/json",
    //   },
    //   data: {
    //     otp: otp,
    //     aadhaarNo: nationalId,
    //     requestId: requestId,
    //     consent: "Y",
    //   },
    // };

    // let response = await axios(config);
    // if (!response.data.result.dataFromAadhaar) {
    //   return res
    //     .status(400)
    //     .send({ status: false, message: "Something went wrong!!" });
    // }
    // let result = response.data.result.dataFromAadhaar;

 

    // let obj = {
    //   national_id: nationalId,
    //   name: result.name,
    //   DOB: result.dob,
    //   address: result.address.combinedAddress,
    // };
    let obj = {
      national_id: nationalId,
      name: "Suraj",
      DOB: "1997-01-29",
      address: "SECTOR-H, JANKIPURAM, Lucknow Uttar Pradesh, India, 226021",
    };
    // console.log(response.data);

    const adhaarExist = await userModel.findOne({ national_id: nationalId });
    if (adhaarExist) {
      return res
        .status(409)
        .send({ status: false, message: "This adhaar already exists" });
    }

    // const farmer = await userModel.create(obj);

    return res
      .status(200)
      .send({
        status: true,
        message: "Adhaar verification successful",
        response: obj,
      });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const farmerPersonalDetails = async function (req, res) {
  try {
  
    let { mobile_number, email_id} = req.body;
    let obj = {};

    if (!mobile_number) {
      return res
        .status(400)
        .send({ status: false, message: "Mobile number is a required field" });
    }

    if (!phoneValidator(mobile_number)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid Phone Number" });
    }
    let numberExist = await userModel.findOne({ mobile_number: mobile_number });
    if (numberExist) {
      return res
        .status(409)
        .send({ status: false, message: "Phone number already exists" });
    }
    obj["mobile_number"] = mobile_number;

    if (!email_id) {
      return res
        .status(400)
        .send({ status: false, message: "Email is Required" });
    }
    if (!validator.isEmail(email_id)) {
      return res
        .status(400)
        .send({ status: false, message: "Enter email in correct format" });
    }
    email_id = email_id.toLowerCase();
    const isDuplicate = await userModel.findOne({ email_id: email_id });
    if (isDuplicate) {
      return res.status(409).send({
        status: false,
        message: `email - ${email_id} already exists`,
      });
    }
    obj["email_id"] = email_id;

    return res
      .status(200)
      .send({ status: true, message: "Details added", data:obj });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const activateProfile = async function (req, res) {
  try {
    if(Object.keys(req.body).length == 0){
      return res
        .status(400)
        .send({ status: false, message: "Fields cannot be empty" });
    }

    console.log(JSON.parse(JSON.stringify(req.body.user_data)))
    
    const userData = JSON.parse(JSON.parse(JSON.stringify(req.body.user_data)))
    console.log(userData)
    const image = req.files;
   
    const {mobile_number, email_id, national_id, name, DOB, address} = userData
    let user = {};
    const wallet = ethers.Wallet.createRandom();
    user["private_key"] = wallet.privateKey;
    user["wallet_address"] = wallet.address;
    user["active_status"] = true;


    if (!mobile_number) {
      return res
        .status(400)
        .send({ status: false, message: "Mobile number is a required field" });
    }
    if (!phoneValidator(mobile_number)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid Phone Number" });
    }
    user["mobile_number"] = mobile_number;

    if (!email_id) {
      return res
        .status(400)
        .send({ status: false, message: "Email is Required" });
    }
    user["email_id"] = email_id;

    if(!national_id){
      return res
        .status(400)
        .send({ status: false, message: "National Id is Required" });
    }
    if(!aadharValidator(national_id)){
      return res.status(400).send({status:false, message:"Enter Valid Adhaar Number"})
    }
    user["national_id"] = national_id

    if(!name){
      return res
        .status(400)
        .send({ status: false, message: "name is Required" });
    }
    user["name"] = name

    if(!DOB){
      return res
        .status(400)
        .send({ status: false, message: "DOB is Required" });
    }
    user["DOB"] = DOB

    if(!address){
      return res
        .status(400)
        .send({ status: false, message: "Address is Required" });
    }
    user["address"] = address

    const todayDate = new Date();
    const dd = String(todayDate.getDate()).padStart(2, "0");
    const mm = String(todayDate.getMonth() + 1).padStart(2, "0"); //January is 0!e
    const yyyy = todayDate.getFullYear();

    const formattedDate = `${dd}/${mm}/${yyyy}`;

    user["onboardedDate"] = formattedDate

    if (image.length == 0) {
      return res
        .status(400)
        .send({ status: false, message: "Bank cheque Image is required" });
    }
    let match = /\.(jpg|jpeg|jfif|pjpeg|pjp|webp|png)$/.test(
      image[0].originalname
    );
    if (match == false) {
      return res.status(400).send({
        status: false,
        message:
          "Bank cheque is required in JPEG/PNG/JPG/JFIF/PJPEG/PJP/WEBP format",
      });
    }
    let uploadedFileURL = await uploadFiles(image[0]);
    console.log(uploadedFileURL);
    user["bank_cheque"] = uploadedFileURL.blobURL;

    const farmer = await userModel.create(user)

    let token = jwt.sign(
      {
        user_id: farmer._id,
        email: farmer.email_id,
        username: farmer.name,
        role: farmer.role,
      },
      JWT.KEY,
      { expiresIn: JWT.EXPIRY }
    );
    return res.status(200).send({
      status: true,
      message: "Profile active",
      data: {
        token: token,
        user_detail: farmer,
      },
    });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const loginFarmer = async function (req, res) {
  try {
    const { mobile_number } = req.body;

    if (!mobile_number) {
      return res
        .status(400)
        .send({ status: false, message: "Mobile number is a required field" });
    }

    if (!phoneValidator(mobile_number)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid Phone Number" });
    }

    const find = await userModel.findOne({ mobile_number: mobile_number });
    if (!find) {
      return res
        .status(404)
        .send({ status: false, message: "User not found!" });
    }

    const otp = randomString.generate({
      length: 6,
      charset: "numeric",
    });

    sendOTP(mobile_number, otp)
      .then(async (data) => {
        console.log(data);
        const OTP_expiry = Date.now() + 300000;
        console.log(OTP_expiry);
        let otpadded = await userModel.findOneAndUpdate(
          { mobile_number: mobile_number },
          { $set: { OTP: otp, OTP_expiry: OTP_expiry } },
          { new: true }
        );
        console.log(otpadded);
        return res.status(200).send({
          status: true,
          message:
            "OTP has been sent to your mobile number " + otpadded.mobile_number,
          data: otpadded,
        });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send({
          status: false,
          message: "Something went wrong! Please try again" + "----" + err,
        });
      });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const verifyfarmerOTP = async function (req, res) {
  try {
    console.log("inside verify farmer")
    let { otp, mobile_number } = req.body;

    if (!otp) {
      return res
        .status(400)
        .send({ status: false, message: "OTP is required" });
    }
    let user = await userModel.findOne({ mobile_number: mobile_number });

    if (!user) {
      return res.status(404).send({ status: false, message: "Register first" });
    }
    if (user.OTP_expiry < Date.now()) {
      user.OTP = null;
      user.OTP_expiry = null;

      await userModel.findOneAndUpdate(
        { mobile_number: mobile_number },
        { $set: user }
      );
      return res.status(400).send({ status: false, message: "Invalid OTP" });
    } else if (otp !== user.OTP) {
      user.OTP = null;
      user.OTP_expiry = null;

      await userModel.findOneAndUpdate(
        { mobile_number: mobile_number },
        { $set: user }
      );
      return res.status(400).send({ status: false, message: "Invalid OTP" });
    } else if (otp == user.OTP && user.OTP_expiry >= Date.now()) {
      user.OTP = null;
      user.OTP_expiry = null;

      let updatedUser = await userModel.findOneAndUpdate(
        { mobile_number: mobile_number },
        { $set: user },
        { new: true }
      );
        console.log(updatedUser)
      let token = jwt.sign(
        {
          user_id: updatedUser._id,
          email: updatedUser.email_id,
          username: updatedUser.name,
          role: updatedUser.role,
        },
        JWT.KEY,
        { expiresIn: JWT.EXPIRY }
      );

      const land = await landModel.find({ farmer: updatedUser._id });
      if (land.length == 0) {
        return res
          .status(404)
          .send({ status: false, message: "No land data found" });
      }
      return res.status(200).send({
        status: true,
        message: "User login successful",
        data: {
          token: token,
          data: updatedUser,
          project: land,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const landDetails = async function (req, res) {
  try {
    if(Object.keys(req.body).length == 0){
      return res
        .status(400)
        .send({ status: false, message: "Fields cannot be empty" });
    }

    const {
      location,
      land_size,
      crops_grown,
      start_time,
      end_time,
      baseLine_co2_emission_rate,
    } = req.body;

    let obj = {};
    obj["farmer"] = req.params.user_id;

    if (!location) {
      return res
        .status(400)
        .send({ status: false, message: "Location is required" });
    }
    if (typeof location !== "string") {
      return res
        .status(400)
        .send({ status: false, message: "Enter valid location" });
    }
    obj["location"] = location;

    if (!land_size) {
      return res
        .status(400)
        .send({ status: false, message: "land size is required" });
    }
    if (typeof land_size !== "number") {
      return res
        .status(400)
        .send({ status: false, message: "Enter valid land size" });
    }
    obj["land_size"] = land_size;

    if (!crops_grown) {
      return res
        .status(400)
        .send({ status: false, message: "type of crop is required" });
    }

    let crops = ["oats", "barley", "corn", "soyabean", "canola", "flaxseed"];
    if (!crops.includes(crops_grown)) {
      return res
        .status(400)
        .send({ status: false, message: "Enter valid crop" });
    }
    obj["crops_grown"] = crops_grown;
    const todayDate = new Date();
    const dd = String(todayDate.getDate()).padStart(2, "0");
    const mm = String(todayDate.getMonth() + 1).padStart(2, "0"); //January is 0!e
    const yyyy = todayDate.getFullYear();

    const formattedDate = `${dd}/${mm}/${yyyy}`;

    obj["added_on"] = formattedDate;

    if (!start_time) {
      return res
        .status(400)
        .send({ status: false, message: "start time is Required" });
    }
    if (!moment(start_time, "DD-MM-YYYY").isValid()) {
      return res.status(400).send({
        status: false,
        message: "Enter valid start time in format DD-MM-YYYY",
      });
    }
    obj["start_time"] = start_time;

    if (!end_time) {
      return res
        .status(400)
        .send({ status: false, message: "end time is Required" });
    }
    if (!moment(end_time, "DD-MM-YYYY").isValid()) {
      return res.status(400).send({
        status: false,
        message: "Enter valid end time in format DD-MM-YYYY",
      });
    }
    obj["end_time"] = end_time;
    obj["baseLine_co2_emission_rate"] = baseLine_co2_emission_rate;

    const land = await landModel.create(obj);

    return res.status(201).send({
      status: true,
      message: "Land Details registered successfully",
      land,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const devicePairing = async function (req, res) {
  try {
    const { land_id } = req.params;

    const { imei_no } = req.body;

    if (!imei_no) {
      return res
        .status(400)
        .send({ status: false, message: "IMEI number is required" });
    }

    const exist = await landModel.findOne({ imei_no: imei_no });
    if (exist) {
      return res.status(400).send({
        statsu: true,
        message: "imei number already exist for any other land",
      });
    }

    let updatedLand = await landModel.findOneAndUpdate(
      { _id: land_id },
      { $set: { imei_no: imei_no } },
      { new: true }
    );
    if (!updatedLand) {
      return res
        .status(400)
        .send({ status: false, message: "land is not registered" });
    }

    cronCreateSensorData(updatedLand._id);
    return res.status(200).send({ status: true, updatedLand });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const resendFarmerOtp = async function (req, res) {
  try {
    const { mobile_number } = req.body;
    const user = await userModel.findOne({ mobile_number: mobile_number });
    if (!user) {
      return res.status(404).send({ status: false, message: "User not found" });
    }
    if (user) {
      const otp = randomString.generate({
        length: 6,
        charset: "numeric",
      });
      let otpsent = sendOTP(user.mobile_number, otp)
        .then(async (data) => {
          console.log(data);
          const OTP_expiry = Date.now() + 300000;
          console.log(OTP_expiry);
          let newUser = await userModel.findOneAndUpdate(
            { mobile_number: mobile_number },
            { $set: { OTP: otp, OTP_expiry: OTP_expiry } },
            { new: true }
          );
          console.log(newUser);
          return res.status(200).send({
            status: true,
            message:
              "OTP has been sent to your mobile number " + user.mobile_number,
            data: newUser,
          });
        })
        .catch((err) => {
          console.log(err);
          return res.status(400).send({
            status: false,
            message: "Something went wrong! Please try again" + "----" + err,
          });
        });
    }
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const carbonTokenization = async function (req, res) {
  try {
    //CarbonCreditContract.balanceOf(farmer metamask address)
    const user_id = req.token.user_id;

    const user = await userModel.findOne({ _id: user_id });
    if (!user) {
    }
    const signer = new ethers.Wallet(user.private_key, provider);
    const carbonCreditTokenContract = new ethers.Contract(
      carbonCreditTokenaddress,
      CarbonCreditTokenABI,
      signer
    );

    let balanceOfFarmer = await carbonCreditTokenContract.balanceOf(
      user.wallet_address
    );
    let balance = parseInt(balanceOfFarmer) / 10 ** 18;
    console.log(balance);

    const updatedBalance = await userModel.findOneAndUpdate(
      { _id: user_id },
      { $set: { balance: balance } },
      { new: true }
    );

    return res
      .status(200)
      .send({ status: true, message: "Balance Of farmer", updatedBalance });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const getCarbonEmissionDetails = async function (req, res) {
  try {
    let { user_id, land_id } = req.params;

    const project = await landModel.findOne({
      _id: land_id,
    });
    if (!project) {
      return res
        .status(404)
        .send({ status: false, message: "No land details found" });
    }

    let pipeline = [
      {
        $lookup: {
          from: "users",
          localField: "farmer",
          foreignField: "_id",
          as: "farmer",
        },
      },
      {
        $match: {
          _id: new objectId(land_id),
        },
      },
      {
        $project: {
          _id: 1.0,
          baseLine_co2_emission_rate: 1.0,
          land_size: 1.0,
          start_time: 1.0,
          end_time: 1.0,
          co2_reduction_diff: 1.0,
          "farmer.name": 1.0,
          "farmer.wallet_address": 1.0,
          "farmer.national_id": 1.0,
          "farmer.DOB": 1.0,
          "farmer.mobile_number": 1.0,
          "farmer.role": 1.0,
        },
      },
    ];

    const details = await landModel.aggregate(pipeline);
    if (details.length == 0) {
      return res.status(404).send({ status: false, message: "No data found" });
    }

    return res
      .status(200)
      .send({ status: true, message: "Carbon emission details", details });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const addLand = async function (req, res) {
  try {

    if(Object.keys(req.body).length == 0){
      return res
        .status(400)
        .send({ status: false, message: "Fields cannot be empty" });
    }
    const {
      no_of_farms,
      lands,
      // location,
      // land_size,
      // crops_grown,
      // start_time,
      // end_time,
      // baseLine_co2_emission_rate,
    } = req.body;

    if (no_of_farms !== lands.length) {
      return res
        .status(400)
        .send({
          status: false,
          message: "No of farms shuld be equal to no of lands being registered",
        });
    }
    let landArr = []
 
    let k= -1
    for(let ele of lands){
      console.log(ele)
      k = k+1
    
      let obj = {};
      obj["farmer"] = req.params.user_id;
  
      if (!ele.location) {
        return res
          .status(400)
          .send({ status: false, message: "Location is required" });
      }
      if (typeof ele.location !== "string") {
        return res
          .status(400)
          .send({ status: false, message: "Enter valid location" });
      }
      obj["location"] = ele.location;
  
      if (!ele.land_size) {
        return res
          .status(400)
          .send({ status: false, message: "Land size is required" });
      }
      if (typeof ele.land_size !== "number") {
        return res
          .status(400)
          .send({ status: false, message: "Enter valid land size" });
      }
      obj["land_size"] = ele.land_size;
  
      if (!ele.crops_grown) {
        return res
          .status(400)
          .send({ status: false, message: "Type of crop grown is required" });
      }
  
      let crops = ["oats", "barley", "corn", "soyabean", "canola", "flaxseed"];
      if (!crops.includes(ele.crops_grown)) {
        return res
          .status(400)
          .send({ status: false, message: "Enter valid crop" });
      }
      obj["crops_grown"] = ele.crops_grown;

      if (!ele.imei_no) {
        return res
          .status(400)
          .send({ status: false, message: "Device ID/IMEI number is required" });
      }
  
      const exist = await landModel.findOne({ imei_no: ele.imei_no });
      if (exist) {
        return res.status(400).send({
          status: true,
          message: `${ele.imei_no} imei number already exist for any other land`,
        });
      }

      for(let i=k+1; i<lands.length; i++){
        if(ele.imei_no === lands[i].imei_no){
          return res.status(409).send({status:false, message:"Imei number for lands can not be same"})
        }
      }

      obj["imei_no"] = ele.imei_no

      const todayDate = new Date();
      const dd = String(todayDate.getDate()).padStart(2, "0");
      const mm = String(todayDate.getMonth() + 1).padStart(2, "0"); //January is 0!e
      const yyyy = todayDate.getFullYear();
  
      const formattedDate = `${dd}/${mm}/${yyyy}`;
  
      obj["added_on"] = formattedDate;
  
      if (!ele.start_time) {
        return res
          .status(400)
          .send({ status: false, message: "Start date is Required" });
      }
      if (!moment(ele.start_time, "DD-MM-YYYY").isValid()) {
        return res.status(400).send({
          status: false,
          message: "Enter valid start time in format DD-MM-YYYY",
        });
      }
      obj["start_time"] = ele.start_time;
  
      if (!ele.end_time) {
        return res
          .status(400)
          .send({ status: false, message: "End date is Required" });
      }
      if (!moment(ele.end_time, "DD-MM-YYYY").isValid()) {
        return res.status(400).send({
          status: false,
          message: "Enter valid end time in format DD-MM-YYYY",
        });
      }
      obj["end_time"] = ele.end_time;
      obj["baseLine_co2_emission_rate"] = ele.baseLine_co2_emission_rate;

      landArr.push(obj)
    }

    await userModel.findOneAndUpdate({_id:req.params.user_id},{$inc:{no_of_farms:no_of_farms}}, {new:true})
    const addedLand = await landModel.insertMany(landArr)

    for(let land of addedLand){
      cronCreateSensorData(land._id);
    }

    return res.status(200).send({status:true, message:"Land added for farmers", data:addedLand})
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

module.exports = {
  registerWithNationaId,
  landDetails,
  devicePairing,
  verifyNationalId,
  farmerPersonalDetails,
  activateProfile,
  loginFarmer,
  verifyfarmerOTP,
  carbonTokenization,
  getCarbonEmissionDetails,
  resendFarmerOtp,
  addLand
};
