const soilDetailModel = require("../models/soilDetailModel");


/**
 * 
 * @param {req}  
 * @param {res}  
 * @returns Data and state of the api
 * @function From mongo obtain the data with UUID and calculate the co2 emission per day
 * 
 */
const npkValue = async function (req, res) {

    try {
        const det = await soilDetailModel.find({ UUID: req.params.UUID });

        // copy the original to temp array
        const tempArr = [...det]

        // convert the timsStamp to format where the time is 00:00:00
        // so that it can be grouped based on day basis
        tempArr.map((ele) => {
            ele.TimeStamp = new Date(ele.TimeStamp)
            ele.TimeStamp = new Date(ele.TimeStamp.getFullYear(), ele.TimeStamp.getMonth(), ele.TimeStamp.getDate())
            ele.TimeStamp = ele.TimeStamp.toDateString()
            // console.log("Time Stamp: ", ele.TimeStamp)
            return tempArr
        })

        // Group based on the day basis:  array of object to objects inside array        
        const groupByDay = tempArr.reduce((group, product) => {
            const { TimeStamp } = product;
            group[TimeStamp] = group[TimeStamp] ?? [];
            group[TimeStamp].push(product);
            return group;
        }, {})


        // obtain keys 
        const cols = Object.keys(groupByDay)


        // obj for storing npk value
        NPK = {}

        // Storing co2 emmision with dates
        // Co2EmissionObj = {}

        // TO structure the object
        // Note the electricity will be updated soon.
        cols.forEach((ky) => {
            NPK[ky] = {
                'SoilNitrogen': 0,
                'SoilPhosphorous': 0,
                'SoilPotassium': 0,
                // 'Eletricity': 26.67,
                // 'Co2Emission': 0
            }
        })
        cols.map((ky) => {

            // length of the day basis for avg calculation
            let lens = groupByDay[ky].length
            const nitro = groupByDay[ky].forEach((ele) => {
                NPK[ky].SoilNitrogen += ele.SoilNitrogen * 1
                NPK[ky].SoilPhosphorous += ele.SoilPhosphorous * 1
                NPK[ky].SoilPotassium += ele.SoilPotassium * 1
                // NPK[ky].Eletricity += ele.Eletricity*1
            })

            // take average on daily basis
            NPK[ky].SoilNitrogen = NPK[ky].SoilNitrogen / lens
            NPK[ky].SoilPhosphorous = NPK[ky].SoilPhosphorous / lens
            NPK[ky].SoilPotassium = NPK[ky].SoilPotassium / lens
            // NPK[ky].Eletricity = NPK[ky].Eletricity/lens
            // console.log("The average: ", NPK[ky].SoilNitrogen)

        });

        // console.log(NPK)

        // convertion to hec/ton and co2 calculation
        Object.keys(NPK).forEach((key) => {

            console.log(NPK[key].SoilNitrogen)
            NPK[key].SoilNitrogen = (NPK[key].SoilNitrogen * 1.4 * 10000) / 1000;
            NPK[key].SoilPhosphorous = NPK[key].SoilPhosphorous * 1.4 * 10
            NPK[key].SoilPotassium = NPK[key].SoilPotassium * 14;
            // co2 calculation
            //NPK[key].Co2Emission = (NPK[key].SoilNitrogen * 5.15) + (NPK[key].SoilPhosphorous * 0.27) + (NPK[key].SoilPotassium * 0.25) + (NPK[key].Eletricity * 0.21233);
            // Co2EmissionObj[key] = NPK[key].Co2Emission
        })

        console.log(NPK)
        console.log("-------------------------------")
        // console.log(Co2EmissionObj)


        return res.status(200).send({
            status: true, data: NPK
        });


    }
    catch (error) {

        return res.status(500).send({
            status: false, data: error
        });

    }
};

module.exports = { npkValue };