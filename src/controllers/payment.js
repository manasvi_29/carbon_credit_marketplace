const Razorpay = require("razorpay");
const crypto = require("crypto");
const { ethers, providers } = require("ethers");
const shortUniqueId = require("short-unique-id");
const mongoose = require("mongoose");
const userModel = require("../models/userModel");
const paymentVerificationModel = require("../models/paymentVerificationModel");
const paymentModel = require('../models/paymentModel')

const genId = async function (req, res) {
  try {
    let { amount } = req.query;
    amount = parseInt(amount);
    if (!amount || amount === NaN) {
      return res.status(400).send({ status: false, message: "Invalid Amount" });
    }
    // const receiptReference = Math.round(Math.random() * 1000);
    const uid = new shortUniqueId({ length: 10 });
    const reciept = uid();
    const razorpay = new Razorpay({
      // key_id: "rzp_test_BlsaL3t3B8bv0l",
      // key_secret: "NJUs9bSaNlkKZ4Smrf53NqRN",
      key_id: "rzp_test_tqMjl7Xp8xaT6u",
      key_secret: "YaRsqkRyXCa8n4eWcHgKpGv3",
      payment_capture: 1,
    });
    console.log(reciept);
    const response = razorpay.orders.create(
      {
        amount: amount,
        currency: "INR",
        receipt: reciept,
      },
      async(err, order) => {
        if (err) {
          return res
            .status(500)
            .send({ status: false, message: "Internal Server Error" });
        } else if (order) {
          console.log(order)
          let obj = {
            // email_id:emailId,
            order_id:order.id,
            amount:order.amount,
            date:Date.now()
          }
          console.log(obj)
          const paymentData = await paymentModel.create(obj)
          console.log(paymentData)
          return res.status(201).send({
            status: true,
            message: "Order Created Successfully",
            data: { ...order },
            paymentData
          });
        }
      }
    );
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const verification = async function (req, res) {
  try {
    const secret = "YaRsqkRyXCa8n4eWcHgKpGv3";
    const {
      razorpay_order_id,
      razorpay_signature,
      razorpay_payment_id,
      amount,
      creatorAddress,
      totalPriceInMatic,
    } = req.body;
   
    console.log(req.body);

    const signature = crypto.createHmac('sha256', secret)
    signature.update(razorpay_order_id + "|" + razorpay_payment_id);
    const generatedSignature = signature.digest('hex');
    
    console.log(generatedSignature, razorpay_signature)

    if (generatedSignature === razorpay_signature) {
      console.log("request is legit");
      
      // we check in database if the said order id has a successful payment
      const order_id_exists = await paymentModel.findOne({
        order_id: razorpay_order_id,
      });
      if (order_id_exists) {
        if (amount < 500) {
          // const provider = new providers.JsonRpcProvider(
          //   "https://polygon-mumbai.g.alchemy.com/v2/UOcasxB4fDBtztvd8qInCSitlXGutl8i"
          // );
          // let admin = await userModel.findOne({ role: "admin" });

          // // Create a decipher object using AES-256-CBC algorithm and the encryption key
          // const decipher = crypto.createDecipheriv(
          //   "aes-256-cbc",
          //   admin.encryption_key,
          //   crypto.randomBytes(16)
          // );

          // // Decrypt the private key using the decipher object
          // let decryptedPrivateKey = decipher.update(
          //   admin.private_key,
          //   "base64",
          //   "utf8"
          // );
          // decryptedPrivateKey += decipher.final("utf8");
          // console.log(decryptedPrivateKey)
          // //now decrypt the private key of admin
          // const wallet = new ethers.Wallet(decryptedPrivateKey, provider);

          // const txObject = {
          //   to: creatorAddress,
          //   value: ethers.utils.parseEther(totalPriceInMatic),
          // };

          // const tx = await wallet.sendTransaction(txObject);

          // console.log(`Transaction hash: ${tx.hash}`);
          console.log("amount is greater less than 500")
        }
        let obj = {
          razorpay_order_id: razorpay_order_id,
          amount: amount,
          creatorAddress: creatorAddress,
          totalPriceInMatic: totalPriceInMatic,
          razorpay_signature: razorpay_signature,
          razorpay_payment_id: razorpay_payment_id,
          date:Date.now()
        };

        // ! All the logic goes here, if the transaction is successful
        let payment = await paymentVerificationModel.create(obj);
        console.log("File written successfully", payment);
        console.log("response", req.body);

        if (amount >= 500) {
          return res.status(200).send({
            status: true,
            message:
              "Transaction have been processed and will be completed by Admin",
          });
        }
        return res
          .status(200)
          .send({ status: true, message: "Payment verified successfully!" });
      } else {
        return res.status(400).send({ status: true, message: "OrderId not found" });
      }
    }return res.status(400).send({status:false,message:"Request is not legit (Invalid Signature)"})
  } catch (error) {
    console.log(error)
    return res.status(500).send({ status: false, Error: error.message });
  }
};

module.exports = { genId, verification };
