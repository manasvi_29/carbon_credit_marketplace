const soilDetailModel = require("../models/soilDetailModel");
const schedule = require('node-schedule')
const moment = require("moment");
const landModel = require("../models/landModel");
const winston = require('winston');
const path = require('path');
const perDayDetailModel = require("../models/perDayDetailModel");

let obj


// Create a Winston logger instance
const logger = winston.createLogger({
  level: 'info', // Set the log level (e.g., 'info', 'debug', 'error')
  format: winston.format.combine(
    winston.format.timestamp(), // Add timestamps to log entries
    winston.format.printf(({ level, message, timestamp }) => {
      return `[${timestamp}] ${level}: ${message}`; // Customize log entry format
    })
  ),
  transports: [
    new winston.transports.File({
      filename: path.join(__dirname, '../logFIle/logs.txt'), // Path to the log file
      maxsize: 5242880, // Maximum log file size (5MB)
      maxFiles: 5, // Maximum number of log files to keep
    }),
  ],
});


const getData = (data) => {
  obj = data
  let res = `UUID: ${obj.UUID}, SIM_IMSI: ${obj.SIM_IMSI}`
  logger.info(res);
  // console.log(obj)
}

const soilDetail = async function () {
  try {
    if (obj == null) return;
    // let obj1 ={
    //   UUID:"866897052753870",
    //   SoilTemperature:"33",
    //   SoilNitrogen:"160",
    //   SoilPhosphorous:"220",
    //   SoilPotassium:"380",
    //   SoilEC:"2052",
    //   SoilPH:"7",
    //   CO2:"718"
    // }
    const {
      UUID,
      SoilTemperature,
      SoilNitrogen,
      SoilPhosphorous,
      SoilPotassium,
      SoilPH,
      SoilEC,
      CO2
    } = obj;
    console.log("sensor data", obj)

    console.log("UUID---",UUID)
    const project = await landModel.findOne({ imei_no: UUID });
    if (!project || project.status == "Completed") {
      return;
    }
    console.log("skipped condition")
    const date = new Date();
    const TimeStamp = date.toISOString().replace('Z', '+00:00');

    console.log(TimeStamp);
    let DateNow = moment().format("YYYY/MM/DD");

    let objStore = {
      project: project._id,
      UUID: UUID,
      TimeStamp: TimeStamp,
      SoilTemperature: parseInt(SoilTemperature),
      SoilNitrogen: parseInt(SoilNitrogen),
      SoilPhosphorous: parseInt(SoilPhosphorous),
      SoilPotassium: parseInt(SoilPotassium),
      SoilPH: parseInt(SoilPH),
      SoilEC: parseInt(SoilEC),
      CO2: parseInt(CO2)
    }

    let present = DateNow.replace(/\//g, '-')
    let Nit = 0, Pho = 0, Pot = 0, Nit_K_Ha, Pho_K_Ha, Pot_K_Ha, Nit_CO2 = 0, Pho_CO2 = 0, Pot_CO2 = 0
    let sum_CO2, obj_CO2 = {}
    let perDayObj = {}
    let data

    console.log(objStore.SoilTemperature === 0)
    //take the data with all non zero values
    if (objStore.SoilTemperature !== 0 && objStore.SoilNitrogen !== 0 && objStore.SoilPhosphorous !== 0 && objStore.SoilPotassium !== 0 && objStore.SoilEC !== 0 && objStore.SoilPH !== 0 && objStore.CO2 !== 0) {
      const allPrevData = await soilDetailModel.find({ UUID: UUID, project: project._id })
      let dateBef = new Date(present)
      dateBef.setDate(dateBef.getDate() - 1)

      console.log("dateToCompare", JSON.stringify(dateBef).split("T")[0].substring(1))
      console.log("102---", allPrevData.length)
      //filter out previous date by one day data from soil details
      const prevData = allPrevData.filter((ele)=>JSON.stringify(ele.TimeStamp).split("T")[0].substring(1) === JSON.stringify(dateBef).split("T")[0].substring(1))
      console.log("prevData", prevData)

      //if previous date data is there and the day is over then perform calculations on the whole data
      if (allPrevData.length !== 0 && prevData.length !== 0) {
        console.log("109---",allPrevData[0])
        console.log("time", JSON.stringify(allPrevData[allPrevData.length - 1].TimeStamp).split("T")[0].substring(1))
        if (JSON.stringify(allPrevData[allPrevData.length - 1].TimeStamp).split("T")[0].substring(1) !== present) {

          //average of the N,P,K values of previous day
          prevData.map((ele) => {
            Nit += ele.SoilNitrogen
            Pho += ele.SoilPhosphorous
            Pot += ele.SoilPotassium
          })
          Nit = parseFloat((Nit / prevData.length).toFixed(2))
            Pho = parseFloat((Pho / prevData.length).toFixed(2))
            Pot =  parseFloat((Pot / prevData.length).toFixed(2))
          console.log("avg", Nit, Pho, Pot)

          //change the average into kg/hectare
          Nit_K_Ha = Nit * 14
          Pho_K_Ha = Pho * 14
          Pot_K_Ha = Pot * 14
          console.log("kg/hc", Nit_K_Ha, Pho_K_Ha, Pot_K_Ha)

          //find CO2 values of all three N,P,K
          Nit_CO2 = Nit_K_Ha * 0.0025
          Pho_CO2 = Pho_K_Ha * 0.0014
          Pot_CO2 = Pot_K_Ha * 0.0011
          console.log("CO2", Nit_CO2, Pho_CO2, Pot_CO2)

          //sum of all CO2 values
          sum_CO2 = Nit_CO2 + Pho_CO2 + Pot_CO2
          sum_CO2 = parseFloat(sum_CO2.toFixed(2))
          console.log("sum_CO2", sum_CO2)

          //saving perday sum CO2 values in an array in land collection
          obj_CO2.sum_CO2 = sum_CO2
          obj_CO2.day = JSON.stringify(prevData[prevData.length - 1].TimeStamp).split("T")[0].substring(1)

          //saving average data of perday
          perDayObj.nitrogen = Nit_K_Ha
          perDayObj.phosphorus = Pho_K_Ha
          perDayObj.potassium = Pot_K_Ha
          perDayObj.cO2 = sum_CO2
          perDayObj.projectId = project._id
          perDayObj.UUID = project.imei_no
          perDayObj.day = JSON.stringify(prevData[prevData.length - 1].TimeStamp).split("T")[0].substring(1)

          let perDayDetail = await perDayDetailModel.create(perDayObj)
          console.log('perDayDetail', perDayDetail)
          await landModel.findOneAndUpdate({ _id: project._id }, { $push: { cO2_arr: obj_CO2 } }, { new: true })
        }

      }

      //create the present coming soil detail data
      data = await soilDetailModel.create(objStore);
      if (project.cO2emitted < objStore.CO2) {
        let newLand = {
          cO2emitted: objStore.CO2,
          co2_reduction_diff: project.baseLine_co2_emission_rate - objStore.CO2
        }
        let updatedLand = await landModel.findOneAndUpdate({ _id: project._id }, { $set: newLand }, { new: true })
        console.log(updatedLand)
      }
      console.log("SoilDetails", data);

    }


    const dateParts = project.end_time.split("/");
    let endtime = `${dateParts[2]}/${dateParts[1]}/${dateParts[0]}`;
    console.log("176---",DateNow, endtime);

    //check if the end time of land or project have been reached then find the maximum CO2 among all days and difference between the NPK values of that day and one day prior and perform calculation
    if (DateNow === endtime || DateNow > endtime) {
      console.log(`cronjob to be closed for ${project._id}`)
      //stop the cronjob
      //make the perday data for last day data
      let newProject

      //proceed only if the data coming from the sensor on end date have non zero values
      if (objStore.SoilTemperature !== 0 && objStore.SoilNitrogen !== 0 && objStore.SoilPhosphorous !== 0 && objStore.SoilPotassium !== 0 && objStore.SoilEC !== 0 && objStore.SoilPH !== 0 && objStore.CO2 !== 0) {

        //perform all the calculations on present data as it'll be last and save that also in per day average data
        let nit_N = (data.SoilNitrogen * 14) * 0.0025
        let pho_P = (data.SoilPhosphorous * 14) * 0.0014
        let pot_K = (data.SoilPotassium * 14) * 0.0011
        let sum = nit_N + pho_P + pot_K
        sum = parseFloat(sum.toFixed(2))

        perDayObj.nitrogen = data.SoilNitrogen * 14
        perDayObj.phosphorus = data.SoilPhosphorous * 14
        perDayObj.potassium = data.SoilPotassium * 14
        perDayObj.cO2 = sum
        perDayObj.projectId = project._id
        perDayObj.UUID = project.imei_no
        perDayObj.day = JSON.stringify(data.TimeStamp).split("T")[0].substring(1)

        let detail = await perDayDetailModel.create(perDayObj)
        console.log("last detail", detail)

        let obj2 = {}
        obj2.sum_CO2 = sum
        obj2.day = JSON.stringify(data.TimeStamp).split("T")[0].substring(1)
        newProject = await landModel.findOneAndUpdate(
          { imei_no: UUID },
          {
            $set: { end_time_reached: true, status: "Completed" },
            $push: { cO2_arr: obj2 }
          },
          { new: true }
        );
      } else {
        //if the NPK values are zero in present coming data then perform the calculations without that data
        newProject = await landModel.findOneAndUpdate(
          { imei_no: UUID },
          { $set: { end_time_reached: true, status: "Completed" } },
          { new: true }
        );
      }

      //after saving last data find the maximum CO2 and the date on that day among all the perday average data
      let max
      let maxDate
      newProject.cO2_arr.map((ele1, i) => {
        if(i+1 < newProject.cO2_arr.length){
          const ele2 = newProject.cO2_arr[i + 1]
        max = ele1.sum_CO2 > ele2.sum_CO2 ? ele1.sum_CO2 : ele2.sum_CO2;
        maxDate = ele1.sum_CO2 > ele2.sum_CO2 ? new Date(ele1.day) : new Date(ele2.day);
        }
      })

      let dayBefore = new Date(maxDate)
      //find the date one day before the date of maximum CO2
      dayBefore.setDate(maxDate.getDate() - 1)
      const prevDay = await perDayDetailModel.find({ projectId: project._id, UUID: project.imei_no }).lean()
      let Nit_diff, Pho_diff, Pot_diff
      console.log("242---",dayBefore, maxDate)

      //find the difference between the N,P,K kg/hectare values on the date with max CO2 and one day before 
      prevDay.map((ele1, i) => {
        if(i+1 < prevDay.length){
        const ele2 = prevDay[i + 1]
        // console.log("ele1, ele2", ele1,ele2)
        if (ele1.day === JSON.stringify(dayBefore).split("T")[0].substring(1)) {
          if (ele2.day === JSON.stringify(maxDate).split("T")[0].substring(1)) {
            Nit_diff = Math.abs(ele1.nitrogen - ele2.nitrogen)
              Pho_diff = Math.abs(ele1.phosphorus - ele2.phosphorus)
              Pot_diff = Math.abs(ele1.potassium - ele2.potassium)
          }
        }
      }
      })
      console.log("258---",Nit_diff, Pho_diff, Pot_diff)

      //find the final CO2 for that land and UUID using difference values of NPK
      let final_CO2 = ((Nit_diff * 5.15) + (Pho_diff * 0.25) + (Pot_diff * 0.027)) / 1000
      final_CO2 = parseFloat(final_CO2.toFixed(2))
      console.log(final_CO2)

      //setting final CO2 and CO2 reduction based on final CO2 in land data
      await landModel.findOneAndUpdate({ _id: newProject._id }, { $set: { final_cO2: final_CO2, co2_reduction_diff: newProject.baseLine_co2_emission_rate - final_CO2 } }, { new: true })
      console.log("cancellation successful")

      //cancel the cron job finally
      cancelScheduledJobs(newProject._id)

    }
  } catch (error) {
    console.log(error);
  }
};


function cancelScheduledJobs(projectId) {
  try {
    console.log("here")

    if (scheduledJobs[projectId]) {

      global.scheduledJobs[projectId].cancel()
      delete global.scheduledJobs[projectId];

    }
  } catch (error) {
    console.log(error)
  }
}



module.exports = { soilDetail, getData };
