const soilDetailModel = require("../models/soilDetailModel");


const avgTemperature = async function(req,res){
    try{
        const {UUID} = req.params
        const data = await soilDetailModel.find({UUID:UUID})
        if(data.length == 0){
            return res.status(404).send({status:false, message:"This UUID have no soil details yet"})
        }
          
          const temperatureByDay = {};
          data.forEach(record => {
            const date = record.createdAt.toISOString().split('T')[0];
            if (!temperatureByDay[date]) {
              temperatureByDay[date] = [];
            }
            temperatureByDay[date].push(parseFloat(record.SoilTemperature));
          });
          
          const averageTemperatureByDay = {};
          
          Object.entries(temperatureByDay).forEach(([date, temperatures]) => {
            const sum = temperatures.reduce((acc, val) => acc + val, 0);
            const average = sum / temperatures.length;
            averageTemperatureByDay[date] = average.toFixed(2);
          });
          
          console.log(averageTemperatureByDay);

          return res.status(200).send({status:true, avgTemperature:averageTemperatureByDay})
          
    }catch(error){
      console.log(error)
        return res.status(500).send({status:false, Error:error.message})
    }
}

module.exports = {avgTemperature}