const userModel = require("../models/userModel");
const validator = require("validator");
const strongPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;
const validName = /^[a-zA-Z0-9_]{3,20}$/;
const {
  aadharValidator,
  drivingLiscenseValidator,
  phoneValidator,
} = require("../utils/validator");
const bcrypt = require("bcrypt");
const moment = require("moment");
const { ethers } = require("ethers");
const randomString = require("randomstring");
const { JWT } = require("../configs/generalConfigs.json");
const jwt = require("jsonwebtoken");
const {
  sendOTP,
  forgotPasswordOTP,
  resetPasswordMsg,
} = require("../aws/services");
const { uploadFiles } = require("../aws/upload");


const signUp = async function (req, res) {
  try {
    let {
      name,
      email_id,
      password,
      national_id,
      driving_liscense,
      DOB,
      mobile_number,
    } = req.body;
    let user = {};
    if (!name) {
      return res
        .status(400)
        .send({ status: false, message: "Name is Required" });
    }
    if (!validName.test(name)) {
      return res.status(400).send({
        status: false,
        message:
          "Enter name in correct format, it can only include _,0-9,a-z and no space or special characters",
      });
    }
    if (!email_id) {
      return res
        .status(400)
        .send({ status: false, message: "Email is Required" });
    }
    if (!validator.isEmail(email_id)) {
      return res
        .status(400)
        .send({ status: false, message: "Enter email in correct format" });
    }
    email_id = email_id.toLowerCase();
    const isDuplicate = await userModel.findOne({
      $or: [{ email_id: email_id }, { name: name }],
    });
    if (isDuplicate) {
      return res.status(409).send({
        status: false,
        message: `email - ${email_id} or username - ${name} already exists`,
      });
    }
    user["name"] = name;
    user["email_id"] = email_id;
    if (!password) {
      return res
        .status(400)
        .send({ status: false, message: "Password is Required" });
    }
    if (!strongPassword.test(password)) {
      return res.status(400).send({
        status: false,
        message:
          "Password should be of minimum 8 and maximum 15 characters also should contain atleast one special character and a number along with aplhabets",
      });
    }
    user["password"] = password;
    const encryptPassword = await bcrypt.hash(password, 10);
    user["coded_password"] = encryptPassword;

    if (!national_id) {
      return res
        .status(400)
        .send({ status: false, message: "National Id is required" });
    }
    if (!aadharValidator(national_id)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid format of Aadhar number" });
    }

    let adhaarExists = await userModel.findOne({ national_id: national_id });
    if (adhaarExists) {
      return res
        .status(409)
        .send({ status: false, message: "Aadhaar number already exists" });
    }
    user["national_id"] = national_id;

    if (!driving_liscense) {
      return res
        .status(400)
        .send({ status: false, message: "Driving Liscense is required" });
    }
    // if (!drivingLiscenseValidator(driving_liscense)) {
    //   return res
    //     .status(400)
    //     .send({ status: false, message: "Invalid format of driving liscense" });
    // }
    // let liscenseExists = await userModel.findOne({
    //     driving_liscense: driving_liscense,
    // });
    // if (liscenseExists) {
    //   return res
    //     .status(409)
    //     .send({ status: false, message: "Driving Liscense already exists" });
    // }
    user["driving_liscense"] = driving_liscense;

    if (!DOB) {
      return res
        .status(400)
        .send({ status: false, message: "DOB is Required" });
    }
    if (!moment(DOB, "DD-MM-YYYY").isValid()) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid Date of Birth" });
    }
    user["DOB"] = DOB;

    if (!phoneValidator(mobile_number)) {
      return res
        .status(400)
        .send({ status: false, message: "Invalid Phone Number" });
    }
    // let numberExist = await userModel.findOne({ mobile_number: mobile_number });
    // if (numberExist) {
    //   return res
    //     .status(409)
    //     .send({ status: false, message: "Phone number already exists" });
    // }
    user["mobile_number"] = mobile_number;

    const todayDate = new Date();
    const dd = String(todayDate.getDate()).padStart(2, "0");
    const mm = String(todayDate.getMonth() + 1).padStart(2, "0"); //January is 0!e
    const yyyy = todayDate.getFullYear();

    const formattedDate = `${dd}/${mm}/${yyyy}`;

    user.onboardedDate = formattedDate;

    const wallet = ethers.Wallet.createRandom();
    user["private_key"] = wallet.privateKey;
    user["wallet_address"] = wallet.address;

    const newUser = await userModel.create(user);
    console.log(newUser);

    if (newUser) {
      const otp = randomString.generate({
        length: 6,
        charset: "numeric",
      });
      // sendOTP(newUser.mobile_number, otp)
      //   .then(async (data) => {
      //     console.log(data);
          const OTP_expiry = Date.now() + 300000;
          console.log(OTP_expiry);
          let otpadded = await userModel.findOneAndUpdate(
            { _id: newUser._id },
            { $set: { OTP: "1234", OTP_expiry: OTP_expiry } },
            { new: true }
          );
          console.log(otpadded);
          return res.status(200).send({
            status: true,
            message:
              "OTP has been sent to your mobile number " +
              otpadded.mobile_number,
            data: otpadded,
          });
        // })
        // .catch((err) => {
        //   console.log(err);
        //   return res.status(400).send({
        //     status: false,
        //     message: "Something went wrong! Please try again" + "----" + err,
        //   });
        // });
    }
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const verifyOTP = async function (req, res) {
  try {
    let { otp } = req.body;
    let { user_id } = req.params;

    if (!otp) {
      return res
        .status(400)
        .send({ status: false, message: "OTP is required" });
    }
    let user = await userModel.findOne({ _id: user_id });
    if (!user) {
      return res.status(404).send({ status: false, message: "register first" });
    }
    if (user.OTP_expiry < Date.now()) {
      user.OTP = null;
      user.OTP_expiry = null;
      user.forgot_password == true
        ? (user.forgot_password = false)
        : (user.forgot_password = null);

      await userModel.findOneAndUpdate({ _id: user_id }, { $set: user });
      return res.status(400).send({ status: false, message: "Invalid OTP" });
    } else if (otp !== user.OTP) {
      user.OTP = null;
      user.OTP_expiry = null;
      user.forgot_password == true
        ? (user.forgot_password = false)
        : (user.forgot_password = null);

      await userModel.findOneAndUpdate({ _id: user_id }, { $set: user });
      return res.status(400).send({ status: false, message: "Invalid OTP" });
    } else if (otp == user.OTP) {
      user.OTP = null;
      user.OTP_expiry = null;
      user.active_status = true;
      if (user.forgot_password == true) user.forgot_password = false;

      await userModel.findOneAndUpdate({ _id: user_id }, { $set: user });
      return res.status(200).send({
        status: true,
        message: "Mobile number verification Successful",
      });
    }
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const resendOTP = async function (req, res) {
  try {
    const { user_id } = req.params;
    const user = await userModel.findOne({ _id: user_id });
    if (!user) {
      return res.status(404).send({ status: false, message: "User not found" });
    }
    if (user) {
      const otp = randomString.generate({
        length: 6,
        charset: "numeric",
      });
      // let otpsent = sendOTP(user.mobile_number, otp)
      // .then(async (data) => {
      //   console.log(data);
      const OTP_expiry = Date.now() + 300000;
      console.log(OTP_expiry);
      let newUser = await userModel.findOneAndUpdate(
        { _id: user._id },
        //sending otp is disabled for a while need to be changed
        { $set: { OTP: "1234", OTP_expiry: OTP_expiry } },
        { new: true }
      );
      console.log(newUser);
      return res.status(200).send({
        status: true,
        message:
          "OTP has been sent to your mobile number " + user.mobile_number,
        data: newUser,
      });
      // })
      // .catch((err) => {
      //   console.log(err);
      //   return res.status(400).send({
      //     status: false,
      //     message: "Something went wrong! Please try again" + "----" + err,
      //   });
      // });
    }
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const login = async function (req, res) {
  try {
    let { username, password } = req.body;
    if (!username) {
      return res
        .status(400)
        .send({ status: false, message: "enter email or username" });
    }
    if(username.includes("@")){
   
      username = username.toLowerCase();
   
    }

    const  user = await userModel.findOne({$or:[{ name: username },{email_id:username}]});

    if(!user){
      return res.status(404).send({status:false, message:"User not found"})
    }
    if(user.blocked === true){
      return res.status(400).send({status:false,message:"Your profile has got Blocked Temporarily"})
    }
    if (!user) {
      return res.status(400).send({
        status: false,
        message: "entered username or email is incorrect",
      });
    }
    if (!password) {
      return res
        .status(400)
        .send({ status: false, message: "Password is Required" });
    }
    let match = await bcrypt.compare(password, user.coded_password);
    if (!match) {
      return res
        .status(400)
        .send({ status: false, message: "Password is Incorrect" });
    }
    if (user.active_status == false) {
      return res.status(400).send({
        status: false,
        message: "Verify Your mobile number First",
        code: "not_verified",
        id: user._id,
      });
    }
    console.log(user);
    let token = jwt.sign(
      {
        user_id: user._id,
        email: user.email_id,
        username: user.name,
        role: user.role,
      },
      JWT.KEY,
      { expiresIn: JWT.EXPIRY }
    );
    return res.status(200).send({
      status: true,
      message: "User login successful",
      data: {
        token: token,
        data: user,
      },
    });
  } catch (error) {
    console.log(error)
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const forgotPassword = async function (req, res) {
  try {
    let { mobile_number } = req.body;
    if (!mobile_number) {
      return res
        .status(400)
        .send({ status: false, message: "enter mobile number" });
    }

    let user = await userModel.findOne({ mobile_number: mobile_number });
    
    if (!user) {
      return res
        .status(404)
        .send({ status: false, message: "mobile number is not registered" });
    }

    if(user.role !== "auditor" && user.role !== "admin"){
      return res.status(403).send({statsu:false, message:"Authorization Failed!!"})
    }
    
    const otp = randomString.generate({
      length: 6,
      charset: "numeric",
    });

    forgotPasswordOTP(user, otp)
      .then(async (data) => {
        console.log(data);
        const OTP_expiry = Date.now() + 300000;
        console.log(OTP_expiry);
        let newUser = await userModel.findOneAndUpdate(
          { _id: user._id },
          { $set: { OTP: otp, OTP_expiry: OTP_expiry, forgot_password: true } },
          { new: true }
        );
        console.log(newUser);
        return res.status(200).send({
          status: true,
          message:
            "OTP has been sent to your mobile number " + newUser.mobile_number,
          data: newUser,
        });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send({
          status: false,
          message: "Something went wrong! Please try again" + "----" + err,
        });
      });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const resetPassword = async function (req, res) {
  try {
    const { password } = req.body;
    const { user_id } = req.params;
    let user = await userModel.findOne({ _id: user_id });

    if (!user) {
      return res.status(404).send({ status: false, message: "user not found" });
    }

    if (!password) {
      return res
        .status(400)
        .send({ status: false, message: "Password is Required" });
    }
    if (!strongPassword.test(password)) {
      return res.status(400).send({
        status: false,
        message:
          "Password should be of minimum 8 and maximum 15 characters also should contain atleast one special character and a number along with aplhabets",
      });
    }
    if (user.forgot_password == false) {
      user.password = password;
      const encryptPassword = await bcrypt.hash(password, 10);
      user.coded_password = encryptPassword;
      user.forgot_password = null;
      resetPasswordMsg(user)
        .then(async (data) => {
          const updatedUser = await userModel.findOneAndUpdate(
            { _id: user_id },
            { $set: user },
            { new: true }
          );
          return res.status(200).send({
            status: true,
            message: "password changed successfully",
            updatedUser,
          });
        })
        .catch((err) => {
          console.log(err);
          return res.status(400).send({
            status: false,
            message: "Something went wrong! Please try again" + "----" + err,
          });
        });
    } else {
      return res
        .status(400)
        .send({ status: false, message: "verify your otp first" });
    }
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const uploadimage = async function (req, res) {
  try {
    let { user_id } = req.params;
    let user = await userModel.findOne({ _id: user_id });
    if (!user) {
      return res.status(404).send({ status: false, message: "User not found" });
    }
    if (user.active_status == false) {
      return res.status(400).send({
        status: false,
        message: "Verify your email first",
        code: "not_verified",
        id: user._id,
      });
    }
    const {files} = req.body;

    if (!files || !files.data) {
      return res
        .status(400)
        .send({ status: false, message: "need image for verification" });
    }
    
    const buffer = Buffer.from(files.data, 'base64');
    let name = user.name + "Image" + "." + files.mime.split("/")[1]
    console.log(name)
    let image = {
      buffer:buffer,
      originalname:name
    }
    console.log(buffer, image)
    
    // console.log(image[0].mimetype.split("/")[1]);
    let match = /^(jpg|jpeg|jfif|pjpeg|pjp|webp|png)$/.test(
      files.mime.split("/")[1]
    );
    if (match == false) {
      return res.status(400).send({
        status: false,
        message: "Image is required in JPEG/PNG/JPG format",
      });
    }
    let uploadedFileURL = await uploadFiles(image);
    console.log(uploadedFileURL);

    let updatedUser = await userModel.findOneAndUpdate(
      { _id: user_id },
      { $set: { face_photo: uploadedFileURL.blobURL } },
      { new: true }
    );
    
    console.log(uploadedFileURL.blobURL);

    return res.status(200).send({ status: true, data: updatedUser });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const generateWallet = async function (req, res) {
  try {
    const { user_id } = req.params;
    const user = await userModel.findOne({ _id: user_id });
    if (!user) {
      return res.status(404).send({ status: false, message: "user not found" });
    }
    return res.status(200).send({
      status: true,
      message: "wallet generation successful",
      publicKey: user.wallet_address,
    });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

const getWalletDetails = async function (req, res) {
  try {
    const { user_id } = req.params;
    const { wallet_address } = req.query;

    const user = await userModel.findOne({ _id: user_id });

    if (!user) {
      return res.status(404).send({ status: false, message: "user not found" });
    }
    console.log(wallet_address)
    if (user.wallet_address !== wallet_address) {
      return res
        .status(400)
        .send({ status: false, message: "invalid public key" });
    }

    return res
      .status(200)
      .send({ status: true, message: "wallet details", user: user,transaction_details:"no transaction to show for now" });
  } catch (error) {
    return res.status(500).send({ status: false, Error: error.message });
  }
};

module.exports = {
  signUp,
  verifyOTP,
  resendOTP,
  login,
  forgotPassword,
  resetPassword,
  uploadimage,
  generateWallet,
  getWalletDetails,
};
