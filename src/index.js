
// 
/*********************************************
 * Importing the necessary packages **********
 *********************************************/

const express = require('express')
const mongoose = require('mongoose')
const {DB_URL}= require('./configs/dbConfigs.json')
const {PORT} = require('./configs/serverConfigs.json')
const {MQTT_URL}= require('./configs/mqttBrokerConfig.json')
const {MQTT_PORT} = require('./configs/mqttConfig.json')
const route = require('./routes/route') 
// const {soilDetail} = require('./controllers/soilDetail')

const multer = require('multer')
const mqtt = require('mqtt')
const cors = require('cors')

const app = express()
const { createAdmin } = require('./controllers/admin')
const { soilDetail, getData } = require('./controllers/soilDetail')
const topic = 'ST/AGRI/SENSOR/DATA/#'
// const clientId = `mqtt_${Math.random().toString(16).slice(3)}`
const connectUrl = `mqtt://${MQTT_URL}:${MQTT_PORT}`


/*********************************************
 ********** MQTT Connection setup ************
 *********************************************/

const client = mqtt.connect(connectUrl, {
  clean: true,
  connectTimeout: 4000,
  reconnectPeriod: 1000,
})

client.on('connect', () => {

  console.log('MQTT Connected')
  client.subscribe([topic], () => {
    console.log(`Subscribe to topic '${topic}'`)
  })

  client.on('message', (topic, payload) => {
    // console.log("!!!!!!!!!!!"+payload)
    getData(JSON.parse(payload.toString()))
    // console.log('Received Message:', topic + ":", payload.toString())
  })

})


app.options('*', cors());
app.use(cors());



// Increase the maximum payload size limit to 50MB
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ limit: '50mb', extended: true }))

app.use(multer().any())


app.use((err, req, res, next) => {
  console.log(err)
  if (err.status === 413) {
    console.log("hello")
    return res.status(413).send({ message: 'File size should be less than 50MB' });
  }
  next();
});

mongoose.connect(DB_URL, {
  useNewUrlParser: true,
})
  .then(() => {
    createAdmin()
    console.log("MongoDb Connected")
  })
  .catch((err) => {
    console.log(err)
})

app.use('/',route)



app.listen(PORT, function () {
  console.log("Express app running on port " + (PORT))
})
