const jwt = require('jsonwebtoken')
const { JWT } = require("../configs/generalConfigs.json");


const authentication = async function(req,res,next){
    try{
        let token = req.headers.authorization || req.headers.Authorization
        // console.log(token)
        if (!token)
        return res
          .status(401)
          .send({ status: false, message: "token must be present" });
        token = token.split(" ")[1]

        let decoded = jwt.verify(token, JWT.KEY, (err,decoded)=>{
            if(err)return res.status(401).send({status:false, error:err.message})
            else{
                req.token = decoded
                next()
            }
        })
    }catch(error){
        console.log(error)
        return res.status(500).send({status:false,Error:error.message})
    }
}

const authorization = async function(req,res,next){
    try{
        const {user_id} = req.params
        const userLoggedIn = req.token.user_id

        if(user_id !== userLoggedIn){
            return res.status(403).send({status:false, message:"You're not authorized to perform this task"})
        }else{
            next()
        }
    }catch(error){
        console.log(error)
        return res.status(500).send({status:false,Error:error.message})
    }
}
module.exports = {authentication,authorization}