const mongoose = require('mongoose')
const objectId = mongoose.Schema.Types.ObjectId


const auditSchema = new mongoose.Schema({
    batch:{
        type:String,
        required:true
    },
    audited_by:{
        type:objectId,
        ref:'users',
        required:true
    },
    status:{
        type:String,
        enum:["Approved","Rejected"],
        required:true
    },
    document:{
        type:String,
        required:true
    },
    blobName:{
        type:String,
        required:true
    },
    document_name:{
        type:String,
        default:"Document by auditor"
    },
    ipfs_hash:{
        type:String
    }
},{timestamps:true})

module.exports = mongoose.model('audit',auditSchema)