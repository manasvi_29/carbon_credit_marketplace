const mongoose = require('mongoose')
const objectId = mongoose.Schema.Types.ObjectId

const batchSchema = new mongoose.Schema({
    batch_id:{
        type:String,
        unique:true
    },
    crop:{
        type:String,
        required:true
    },
    farmers:{
        type:Number,
        requried:true
    },
    farmers_arr:[String],
    batch_ipfs_hash:{
        type:String,
        required:true
    },
    co2emission:{
        type:Number,
        required:true
    },
    co2TotalReduction:{
        type:Number,
        required:true
    },
    co2BaselineTotal:{
        type:Number,
        required:true
    },
    startTime:{
        type:String
    },
    endTime:{
        type:String
    },
    batch_project_id:{  //projectid is coming from blockchain
        type:Number
    },
    batch_claim_id:{
        type:Number
    },
    status:{
        type:String,
        enum:["Approved", "Rejected", "New", "Pending"],
        default: "New"
    },
    audited_by:{
        type:objectId
    },
    green_nft_address:{
        type:String
    },
    auditor_address:{
        type:String
    },
    owner_balance:{
        type:Number
    },
    audited_report:{
        type:String
    },
    claim_id:{
        type:Number
    },
    farmer_address:{
        type:String
    },
    issued_date:{
        type:Number
    },
    green_nft_status:{
        type:Number
    },
    carbon_credits:{
        type:Number
    },
    buyable_carbon_credits:{
        type:Number
    },
    nftId:{
        type:Number
    }
})


module.exports = mongoose.model('batch',batchSchema)

