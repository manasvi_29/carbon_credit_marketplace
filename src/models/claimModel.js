const mongoose = require('mongoose')
const objectId = mongoose.Schema.Types.ObjectId

const claimSchema = new mongoose.Schema({
    claim_id:{
        type:Number,
        required:true
    },
    project:{
        type:objectId,
        ref:'projects',
        required:true
    },
    co2_emission_rate:{
        type:String,
        required:true
    },
    co2Reductions:{
        type:Number,
        required:true
    },
    ref_doc:{
        type:String,
        required:true
    },
    start_period:{
        type:String,
        required:true
    },
    end_period:{
        type:String,
        required:true
    }
},{timestamps:true})

module.exports = mongoose.model('claim',claimSchema)