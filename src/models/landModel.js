const mongoose = require('mongoose')
const objectId = mongoose.Schema.Types.ObjectId


const landSchema = new mongoose.Schema({
    farmer :{
        type:objectId,
        ref:'user',
        required:true,
    },
    location:{
        type:String,
        required:true
    },
    land_size:{
        type:Number,
        required:true
    },
    crops_grown:{
        type:String,
        enum:["oats","barley","corn","soyabean","canola","flaxseed","maize","jowar","banana","lemon","bamboo"]
    },
    imei_no:{
        type:String,
        default:null
    },  
    baseLine_co2_emission_rate:{
        type:Number
    },
    cO2emitted:{
        type:Number,
        default:0
    },
    co2_reduction_diff:{
        type:Number,
        default:0
    },
    cO2_arr:{
        type: [{
            sum_CO2:Number,
            day:String
        }],
        default: []
    },
    final_cO2:{
        type:Number
    },
    added_on:{
        type:String,
        required:true
    },
    ipfs_hash:{
        type:String,
        default:null
    },
    status:{
        type:String,
        enum:["Completed","Pending"],
        default:"Pending"
    },
    audited_by:{
        type:objectId,
        ref:'user',
        default:null
    },
    start_time:{
        type:String,
        required:true
    },
    end_time:{
        type:String,
        required:true
    },
    end_time_reached:{
        type:Boolean,
        default:false
    },
    set_in_batch:{
        type:Boolean,
        default:false
    }
},{timestamps:true})


module.exports = mongoose.model('land',landSchema)
