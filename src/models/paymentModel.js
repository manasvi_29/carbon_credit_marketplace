const mongoose = require('mongoose')

const paymentSchema = new mongoose.Schema({
    email_id:String,
    order_id:String,
    currency:{
        type:String,
        defaule:"INR"
    },
    amount:Number,
    date:String
},{timestamps:true})

module.exports = mongoose.model('payment',paymentSchema)