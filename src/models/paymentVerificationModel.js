const mongoose = require('mongoose')

const paymentVerificationSchema = new mongoose.Schema({
    razorpay_order_id:String,
    amount:Number,
    creatorAddress:String,
    totalPriceInMatic:String,
    razorpay_signature:String,
    razorpay_payment_id:String,
    date:String
},{timestamps:true})

module.exports = mongoose.model('paymentVerif',paymentVerificationSchema)