const mongoose = require('mongoose')

const perDayDetailSchema = new mongoose.Schema({
    nitrogen:{
        type:Number,
        required:true
    },
    phosphorus:{
        type:Number,
        required:true
    },
    potassium:{
        type:Number,
        required:true
    },
    cO2:{
        type:Number,
        required:true
    },
    projectId:{
        type:mongoose.Schema.Types.ObjectId,
        required:true
    },
    UUID:{
        type:String,
        required:true
    },
    day:{
        type:String,
        required:true
    }
})

module.exports = mongoose.model('perDayDetail', perDayDetailSchema)