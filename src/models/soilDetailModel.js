const mongoose = require('mongoose')
const objectId = mongoose.Schema.Types.ObjectId

const soilDetailSchema = new mongoose.Schema({
    project:{
        type:objectId,
        ref:"lands"
    },
    UUID:{
        type:String,
    },
    TimeStamp:{
        type:Date,
    },
    SoilTemperature:{
        type:Number,

    },
    SoilNitrogen:{
        type:Number,

    },
    SoilPhosphorous:{
        type:Number,
   
    },
    SoilPotassium:{
        type:Number,

    },
    SoilPH:{
        type:Number,

    },
    SoilEC:{
        type:Number
    },
    CO2:{
        type:Number
    }
},{timestamps:true})

module.exports = mongoose.model('soilDetail',soilDetailSchema)