const mongoose = require('mongoose')
const objectId = mongoose.Schema.Types.ObjectId


const transactionSchema = new mongoose.Schema({
    farmer:{
        type:objectId,
        required:true
    },
    wallet_address:{
        type:String,
        required:true
    },
    no_of_carbonCredit:{
        type:Number,
        required:true
    },
    isPaymentCompleted:{
        type:Boolean,
        required:true
    },
    txn_hash:{
        type:String,
        required:true
    }
},{timestamps:true})


module.exports = mongoose.model('transaction', transactionSchema)
