const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    national_id:{
        type:String,
        default:null,
        maxLength:12
    },
    name:{
        type:String,
        required:true
    },
    email_id:{
        type:String,
    },
    password:{
        type:String,
        maxLength:15
    },
    coded_password:{
        type:String,

    },
    OTP:{
        type:String,
        default:null
    },
    OTP_expiry:{
        type:Date,
        default:null
    },
    active_status:{
        type:Boolean,
        default:false
    },
    DOB:{
        type:String,
        default:null,
        maxLength:13
    },
    mobile_number:{
        type:String,
        default:null
    },
    forgot_password:{
        type:Boolean,
        default:null
    },
    address:{
        type:String,
        default:null
    },
    private_key:{
        type:String,
    },
    wallet_address:{
        type:String
    },
    //admin
    encryption_key:{
        type:String
    },
    organization:{
        type:String
    },
    bank_cheque:{
        type:String
    },
    //
    onboardedDate:{
        type:String,
        required:true
    },
    role:{
        type:String,
        default:"farmer"
    },
    blocked:{
        type:Boolean,
        default:false
    },
    balance:{
        type:Number
    },
    payment_status:{
        type:Boolean,
        default:false
    },
    carbon_credits:{
        type:Number,
        default:0
    },
    no_of_farms:{
        type:Number,
        default:0
    }
},{timestamps:true})


module.exports = mongoose.model('user',userSchema)