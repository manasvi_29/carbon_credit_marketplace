/******************************
 * Importing the necessary packages
 ******************************/

const express = require("express");
const {
  getParticularProject,
  searchProject,
  getAllProjects,
} = require("../controllers/common");
const { authentication, authorization } = require("../middlewares/auth");
const {
  verifyOTP,
  resendOTP,
  login,
  forgotPassword,
  resetPassword,
  generateWallet,
  getWalletDetails,
} = require("../controllers/user");
const { genId, verification } = require("../controllers/payment");
const {
  createAuditor,
  adminKeys,
  adminDashboard,
  auditorDetails,
  farmerDetails,
  totalCount,
  blockUser,
  allProject,
  unblockUser,
  filterProjects,
  createBatchOfProjects,
  updateBatch,
  getAllBatch,
  getFarmersFromBatch,
  filterBatchByStatus,
  changeBatchToPending,
  approvedbatchData,
  transactionHistory,
  getTransactionHistory,
  checkUniqueValuesForAuditor,
} = require("../controllers/admin");
const {
  statusChange,
  auditClaim,
  getCLaimData,
  getParticularBatch,
} = require("../controllers/auditor");
const {
  registerWithNationaId,
  landDetails,
  devicePairing,
  verifyNationalId,
  farmerPersonalDetails,
  activateProfile,
  loginFarmer,
  verifyfarmerOTP,
  carbonTokenization,
  getCarbonEmissionDetails,
  resendFarmerOtp,
  addLand,
} = require("../controllers/farmerController");
const soilDetailModel = require("../models/soilDetailModel");

// co2Emission and npk value for charts ploting.
const { co2Emission } = require('../controllers/co2Emission')
const { npkValue } = require('../controllers/npkValue')
const { co2 } = require('../controllers/co2')
const { avgTemperature } = require('../controllers/temperature')
const { CO2Cal } = require('../controllers/CO2Cal')
const { NPK } = require('../controllers/NPK')
const { Tempp } = require('../controllers/Tempp')
const { ccc } = require('../controllers/ccc')
const { mob_npk } = require('../controllers/mob_npk');
const landModel = require("../models/landModel");
const { cronCreateSensorData } = require("../utils/cronJob");
const { soilPh } = require("../controllers/soilPh");
const { atmos } = require("../controllers/atmos");
// const { calculation, func } = require("../controllers/calculation");


const router = express.Router()


//routes for farmers
// router.post('/signUp',signUp)
router.post("/user/:user_id/verification", verifyOTP);
router.post("/user/:user_id/resend", resendOTP);

//for auditor and farmer and all other roles
router.post("/login", login);

router.post("/forgotPassword", forgotPassword);
router.put("/user/:user_id/resetPassword", resetPassword);
router.get(
  "/user/:user_id/generate_wallet",
  authentication,
  authorization,
  generateWallet
);
router.get(
  "/user/:user_id/get_wallet_details",
  authentication,
  authorization,
  getWalletDetails
);
router.get("/genId", genId);
router.post("/verification", verification);

//for auditor and farmer and all other roles
router.get("/user/:user_id/all_projects", authentication, getAllProjects);
router.get(
  "/user/:user_id/all_projects/:project_id",
  authentication,
  getParticularProject
);
router.get(
  "/user/:user_id/search_projects",
  authentication,
  authorization,
  searchProject
);

//admin(web app)
router.post(
  "/user/:user_id/validate_auditor",
  authentication,
  authorization,
  checkUniqueValuesForAuditor
);
router.post(
  "/user/:user_id/create_auditor",
  authentication,
  authorization,
  createAuditor
);
router.post(
  "/user/:user_id/add_address",
  authentication,
  authorization,
  adminKeys
);
router.get(
  "/user/:user_id/admin_dashboard",
  authentication,
  authorization,
  adminDashboard
);
router.get(
  "/user/:user_id/:farmer_id/projects",
  authentication,
  authorization,
  allProject
);
router.get(
  "/user/:user_id/userDetails/auditors_list",
  authentication,
  authorization,
  auditorDetails
);
router.get(
  "/user/:user_id/userDetails/farmers_list",
  authentication,
  authorization,
  farmerDetails
);
router.get(
  "/user/:user_id/userDetails/users_count",
  authentication,
  authorization,
  totalCount
);
router.put(
  "/user/:user_id/:block_id/block_user",
  authentication,
  authorization,
  blockUser
);
router.put(
  "/user/:user_id/:block_id/unblock_user",
  authentication,
  authorization,
  unblockUser
);
router.get(
  "/user/:user_id/filter_project",
  authentication,
  authorization,
  filterProjects
);
router.post(
  "/user/:user_id/create_batch",
  authentication,
  authorization,
  createBatchOfProjects
);
router.put(
  "/user/:user_id/batch_Data/:batch_id",
  authentication,
  authorization,
  updateBatch
);
router.get(
  "/user/:user_id/all_batch_data",
  authentication,
  authorization,
  getAllBatch
);
router.get(
  "/user/:user_id/all_batch_data/:batch_id/batch_farmers",
  authentication,
  authorization,
  getFarmersFromBatch
);
router.get(
  "/user/:user_id/batch_by_status",
  authentication,
  authorization,
  filterBatchByStatus
);
router.put(
  "/user/:user_id/all_batch/:batch_id",
  authentication,
  authorization,
  changeBatchToPending
);
router.get(
  "/user/:user_id/approved_batches",
  authentication,
  authorization,
  approvedbatchData
);
router.post(
  "/user/:user_id/transaction_data",
  authentication,
  authorization,
  transactionHistory
);
router.get(
  "/user/:user_id/get_transaction_history/:wallet_address",
  authentication,
  authorization,
  getTransactionHistory
);

//auditor
router.post(
  "/user/:user_id/:batch_id/change_status",
  authentication,
  authorization,
  statusChange
);
router.post(
  "/user/:user_id/:batch_id/audit_claim",
  authentication,
  authorization,
  auditClaim
);
router.get(
  "/user/:user_id/:batch_id/get_claim_data",
  authentication,
  authorization,
  getCLaimData
);
router.get(
  "/user/:user_id/all_batch_data/:batch_id",
  authentication,
  authorization,
  getParticularBatch
);

//farmer apis
router.post("/signup", registerWithNationaId);
router.post("/signup/verify_adhaar", verifyNationalId);
router.post("/signup/farmer_details", farmerPersonalDetails);
router.post("/signup/activate_profile", activateProfile);
router.post("/login_farmer", loginFarmer);
router.post("/verify_otp", verifyfarmerOTP);
router.post(
  "/user/:user_id/land_details",
  authentication,
  authorization,
  landDetails
);
router.post(
  "/user/:user_id/add_land",
  authentication,
  authorization,
  addLand
);
router.post('/user/:user_id/:land_id/land_details/device_pairing', devicePairing)
router.get(
  "/user/:user_id/carbon_tokenization",
  authentication,
  authorization,
  carbonTokenization
);
router.get(
  "/user/:user_id/:land_id/emission_review",
  authentication,
  authorization,
  getCarbonEmissionDetails
);
router.post("/resendFarmerOtp", resendFarmerOtp);
// router.post('/addLand', addLand)

router.get("/soilDetails/:UUID", (req, res) => {
  soilDetailModel
    .find({ UUID: req.params.UUID })
    .then((data) => {
      // console.log(data)
      res.send(data);
    })
    .catch((err) => {
      console.log("error" + err);
    });
});

// api for daily co2emmision 
// router.get('/co2EmissionVal/:UUID', CO2Cal);
router.get('/npk/:UUID', NPK);
router.get('/co2/:UUID', CO2Cal);
router.get('/temperature/:UUID', Tempp)
router.get('/ccc/:UUID', ccc)
router.get('/mob/npk/:UUID', mob_npk)
router.get('/ph/:UUID', soilPh)
router.get('/atmos_co2/:UUID', atmos)

// router.post('/calculation',func)
router.get('/start_cron', async function(req,res){
  try{
    const lands = await landModel.find()
    for(let land of lands){
  
      cronCreateSensorData(land._id)
    }
    return res.status(200).send({status:true, message:"cronjob started for all the projects",data:lands})
  }catch(error){
    return res.status(500).send({status:false, Error:error.message})
  }
  
})

router.get('/start_particular_cron/:landId', async function(req,res){
  try{
    const land = await landModel.findOne({_id:req.params.landId})
    if(!land){
      return res.status(404).send({status:false, message:"Land not found"})
    }
  
    cronCreateSensorData(land._id)
    
    return res.status(200).send({status:true, message:"cronjob started",data:land})
  }catch(error){
    return res.status(500).send({status:false, Error:error.message})
  }
  
})


router.all("/*", function (req, res) {
  return res.status(404).send({ status: false, message: "Page Not Found" });
});

module.exports = router;
