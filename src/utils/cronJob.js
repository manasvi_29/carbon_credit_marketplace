const schedule = require('node-schedule')
const { soilDetail } = require('../controllers/soilDetail')

global.scheduledJobs = {}; 



const cronCreateSensorData = async function(projectId){
    // const job = schedule.scheduleJob('0 * * * *',soilDetail)
    
    // const job = schedule.scheduleJob('*/15 * * * *',soilDetail)
    const job = schedule.scheduleJob('*/1 * * * *',soilDetail)
    global.scheduledJobs[projectId] = job
    console.log(`Cronjob started for ${projectId}`)
}




module.exports = {cronCreateSensorData}