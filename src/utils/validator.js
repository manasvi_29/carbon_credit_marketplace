function aadharValidator(aadhar){
    let validAadhar = /^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$/
    let res =  validAadhar.test(aadhar)  
    return res
}

function drivingLiscenseValidator(liscense){
    let validLiscense = /^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$/
    return validLiscense.test(liscense)
}

function phoneValidator(number){
    let validPhone =  /^[6-9]\d{9}$/
    console.log(validPhone.test(number))
    return validPhone.test(number)
}

function isValidNumber(value) {
    if (typeof value !== 'number') {
      value = Number(value);
    }
    return !Number.isNaN(value) && Number.isFinite(value);
  }


module.exports = {aadharValidator, drivingLiscenseValidator,phoneValidator,isValidNumber}